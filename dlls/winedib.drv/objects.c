/*
 * DIB Engine Objects
 *
 * Copyright 2008 Huw Davies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>
#include <stdlib.h>

#include "windef.h"
#include "winbase.h"
#include "wingdi.h"
#include "wine/winbase16.h" /* GlobalLock16 */

#include "dibeng.h"

#include "wine/debug.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibeng);

/*
 *
 * Decompose the 16 ROP2s into an expression of the form
 *
 * D = (D & A) ^ X
 *
 * Where A and X depend only on P (and so can be precomputed).
 *
 *                                       A    X
 *
 * R2_BLACK         0                    0    0
 * R2_NOTMERGEPEN   ~(D | P)            ~P   ~P
 * R2_MASKNOTPEN    ~P & D              ~P    0
 * R2_NOTCOPYPEN    ~P                   0   ~P
 * R2_MASKPENNOT    P & ~D               P    P
 * R2_NOT           ~D                   1    1
 * R2_XORPEN        P ^ D                1    P
 * R2_NOTMASKPEN    ~(P & D)             P    1
 * R2_MASKPEN       P & D                P    0
 * R2_NOTXORPEN     ~(P ^ D)             1   ~P
 * R2_NOP           D                    1    0
 * R2_MERGENOTPEN   ~P | D               P   ~P
 * R2_COPYPEN       P                    0    P
 * R2_MERGEPENNOT   P | ~D              ~P    1
 * R2_MERGEPEN      P | D               ~P    P
 * R2_WHITE         1                    0    1
 *
 */

/* A = (P & A1) | (~P & A2) */
#define ZERO {0, 0}
#define ONE {0xffffffff, 0xffffffff}
#define P {0xffffffff, 0}
#define NOT_P {0, 0xffffffff}

static const DWORD rop2_and_array[16][2] =
{
    ZERO, NOT_P, NOT_P, ZERO,
    P,    ONE,   ONE,   P,
    P,    ONE,   ONE,   P,
    ZERO, NOT_P, NOT_P, ZERO
};

/* X = (P & X1) | (~P & X2) */
static const DWORD rop2_xor_array[16][2] =
{
    ZERO, NOT_P, ZERO, NOT_P,
    P,    ONE,   P,    ONE,
    ZERO, NOT_P, ZERO, NOT_P,
    P,    ONE,   P,    ONE
};

#undef NOT_P
#undef P
#undef ONE
#undef ZERO

void calc_and_xor_masks(INT rop, DWORD color, DWORD *and, DWORD *xor)
{
    /* NB The ROP2 codes start at one and the arrays are zero-based */
    *and = (color & rop2_and_array[rop-1][0]) | ((~color) & rop2_and_array[rop-1][1]);
    *xor = (color & rop2_xor_array[rop-1][0]) | ((~color) & rop2_xor_array[rop-1][1]);
}

static const dash_pattern_t dash_patterns[4] =
{
    {2, {18, 6}},
    {2, {3,  3}},
    {4, {9, 6, 3, 6}},
    {6, {9, 3, 3, 3, 3, 3}}
};

static inline void order_end_points(int *s, int *e)
{
    if(*s > *e)
    {
        int tmp;
        tmp = *s + 1;
        *s = *e + 1;
        *e = tmp;
    }
}

static void solid_pen_hline(dibeng_pdevice_t *pdev, INT start, INT end, INT row)
{
    order_end_points(&start, &end);
    pdev->dib.funcs->solid_hline(&pdev->dib, start, end, row, pdev->pen_and, pdev->pen_xor);
}

static void solid_pen_vline(dibeng_pdevice_t *pdev, INT col, INT start, INT end)
{
    order_end_points(&start, &end);
    pdev->dib.funcs->solid_vline(&pdev->dib, col, start, end, pdev->pen_and, pdev->pen_xor);
}


static void WINAPI solid_pen_line_callback(INT x, INT y, LPARAM lparam)
{
    dibeng_pdevice_t *pdev = (dibeng_pdevice_t *)lparam;

    pdev->dib.funcs->set_pixel(&pdev->dib, x, y, pdev->pen_and, pdev->pen_xor);
    return;
}

static void solid_pen_line(dibeng_pdevice_t *pdev, INT x1, INT y1, INT x2, INT y2)
{
    LineDDA(x1, y1, x2, y2, solid_pen_line_callback, (LPARAM)pdev);
}

static inline void get_dash_colors(dibeng_pdevice_t *pdev, DWORD *and, DWORD *xor)
{
    if(pdev->mark_space == mark)
    {
        *and = pdev->pen_and;
        *xor = pdev->pen_xor;
    }
    else if(GetBkMode(pdev->hdc) == OPAQUE)
    {
        *and = pdev->bkgnd_and;
        *xor = pdev->bkgnd_xor;
    }
    else
    {
        *and = 0xffffffff;
        *xor = 0;
    }
}

static inline void next_dash(dibeng_pdevice_t *pdev)
{
    if(pdev->left_in_dash != 0) return;

    pdev->cur_dash++;
    if(pdev->cur_dash == pdev->pen_pattern->count) pdev->cur_dash = 0;
    pdev->left_in_dash = pdev->pen_pattern->dashes[pdev->cur_dash];
    if(pdev->mark_space == mark) pdev->mark_space = space;
    else pdev->mark_space = mark;
}

static void dashed_pen_hline(dibeng_pdevice_t *pdev, INT start, INT end, INT row)
{
    INT cur_x = start;
    DWORD and, xor;
    DWORD dash_len;

    if(start <= end)
    {
        while(cur_x != end)
        {
            get_dash_colors(pdev, &and, &xor);

            dash_len = pdev->left_in_dash;
            if(cur_x + dash_len > end)
                dash_len = end - cur_x;

            pdev->dib.funcs->solid_hline(&pdev->dib, cur_x, cur_x + dash_len, row, and, xor);
            cur_x += dash_len;

            pdev->left_in_dash -= dash_len;
            next_dash(pdev);
        }
    }
    else
    {
        while(cur_x != end)
        {
            get_dash_colors(pdev, &and, &xor);

            dash_len = pdev->left_in_dash;
            if(cur_x - (INT)dash_len < end)
                dash_len = cur_x - end;

            pdev->dib.funcs->solid_hline(&pdev->dib, cur_x - dash_len + 1, cur_x + 1, row, and, xor);
            cur_x -= dash_len;

            pdev->left_in_dash -= dash_len;
            next_dash(pdev);
        }
    }
}

static void dashed_pen_vline(dibeng_pdevice_t *pdev, INT col, INT start, INT end)
{
    INT cur_y = start;
    DWORD and, xor;
    DWORD dash_len;

    if(start <= end)
    {
        while(cur_y != end)
        {
            get_dash_colors(pdev, &and, &xor);

            dash_len = pdev->left_in_dash;
            if(cur_y + dash_len > end)
                dash_len = end - cur_y;

            pdev->dib.funcs->solid_vline(&pdev->dib, col, cur_y, cur_y + dash_len, and, xor);
            cur_y += dash_len;

            pdev->left_in_dash -= dash_len;
            next_dash(pdev);
        }
    }
    else
    {
        while(cur_y != end)
        {
            get_dash_colors(pdev, &and, &xor);

            dash_len = pdev->left_in_dash;
            if(cur_y - (INT)dash_len < end)
                dash_len = cur_y - end;

            pdev->dib.funcs->solid_vline(&pdev->dib, col, cur_y - dash_len + 1, cur_y + 1, and, xor);
            cur_y -= dash_len;

            pdev->left_in_dash -= dash_len;
            next_dash(pdev);
        }
    }
}

static void WINAPI dashed_pen_line_callback(INT x, INT y, LPARAM lparam)
{
    dibeng_pdevice_t *pdev = (dibeng_pdevice_t *)lparam;
    DWORD and, xor;

    get_dash_colors(pdev, &and, &xor);

    pdev->dib.funcs->set_pixel(&pdev->dib, x, y, and, xor);

    pdev->left_in_dash--;
    next_dash(pdev);

    return;
}

static void dashed_pen_line(dibeng_pdevice_t *pdev, INT x1, INT y1, INT x2, INT y2)
{
    LineDDA(x1, y1, x2, y2, dashed_pen_line_callback, (LPARAM)pdev);
}

void reset_dash_origin(dibeng_pdevice_t *pdev)
{
    pdev->cur_dash = 0;
    if(pdev->pen_pattern)
        pdev->left_in_dash = pdev->pen_pattern->dashes[0];
    pdev->mark_space = mark;
}


/* For 1bpp bitmaps, unless the selected foreground color exactly
   matches one of the colors in the colortable, then the color that
   isn't the bkgnd color is used. */
static DWORD adjust_fg_color(dibeng_pdevice_t *pdev, COLORREF color)
{
    RGBQUAD rgb;
    int i;

    rgb.rgbRed   = GetRValue(color);
    rgb.rgbGreen = GetGValue(color);
    rgb.rgbBlue  = GetBValue(color);

    for(i = 0; i < pdev->dib.color_table_size; i++)
    {
        RGBQUAD *cur = pdev->dib.color_table + i;
        if((rgb.rgbRed == cur->rgbRed) && (rgb.rgbGreen == cur->rgbGreen) && (rgb.rgbBlue == cur->rgbBlue))
            return i;
    }
    return ~pdev->bkgnd_color & 1;
}

static void fixup_fg_colors_1(dibeng_pdevice_t *pdev)
{
    INT rop = GetROP2(pdev->hdc);

    pdev->pen_color   = adjust_fg_color(pdev, pdev->pen_colorref);
    pdev->brush_color = adjust_fg_color(pdev, pdev->brush_colorref);

    calc_and_xor_masks(rop, pdev->pen_color, &pdev->pen_and, &pdev->pen_xor);
    calc_and_xor_masks(rop, pdev->brush_color, &pdev->brush_and, &pdev->brush_xor);
    HeapFree(GetProcessHeap(), 0, pdev->brush_and_bits);
    HeapFree(GetProcessHeap(), 0, pdev->brush_xor_bits);
    pdev->brush_and_bits = NULL;
    pdev->brush_xor_bits = NULL;
}

/***********************************************************************
 *           SelectPen
 */
HPEN DIBENG_SelectPen( dibeng_pdevice_t *pdev, HPEN hpen )
{
    LOGPEN logpen;

    GetObjectW(hpen, sizeof(logpen), &logpen);

    pdev->pen_colorref = logpen.lopnColor;

    if(pdev->dib.bit_count == 1)
        fixup_fg_colors_1(pdev);
    else
        pdev->pen_color = pdev->dib.funcs->colorref_to_pixel(&pdev->dib, logpen.lopnColor);

    calc_and_xor_masks(GetROP2(pdev->hdc), pdev->pen_color, &pdev->pen_and, &pdev->pen_xor);

    switch(logpen.lopnStyle)
    {
    default:
        FIXME("Unhandled pen style %d\n", logpen.lopnStyle);
        /* fall through */
    case PS_SOLID:
        pdev->pen_hline = solid_pen_hline;
        pdev->pen_vline = solid_pen_vline;
        pdev->pen_line  = solid_pen_line;
        pdev->pen_pattern = NULL;
        break;

    case PS_DASH:
    case PS_DOT:
    case PS_DASHDOT:
    case PS_DASHDOTDOT:
        pdev->pen_hline = dashed_pen_hline;
        pdev->pen_vline = dashed_pen_vline;
        pdev->pen_line  = dashed_pen_line;
        pdev->pen_pattern = &dash_patterns[logpen.lopnStyle - PS_DASH];
        reset_dash_origin(pdev);
        break;
    }

    return hpen;

}

static void solid_brush_hline(dibeng_pdevice_t *pdev, INT start, INT end, INT row)
{
    order_end_points(&start, &end);
    pdev->dib.funcs->solid_hline(&pdev->dib, start, end, row, pdev->brush_and, pdev->brush_xor);
}


static void generate_masks(dibeng_pdevice_t *pdev, dib_info_t *dib, void **and, void **xor)
{
    INT rop = GetROP2(pdev->hdc);
    DWORD *color_ptr, *and_ptr, *xor_ptr;
    DWORD size = dib->height * abs(dib->stride);
    *and = HeapAlloc(GetProcessHeap(), 0, size);
    *xor = HeapAlloc(GetProcessHeap(), 0, size);

    color_ptr = dib->bits;
    and_ptr = *and;
    xor_ptr = *xor;

    while(size)
    {
        calc_and_xor_masks(rop, *color_ptr++, and_ptr++, xor_ptr++);
        size -= 4;
    }
}

static void pattern_brush_hline(dibeng_pdevice_t *pdev, INT start, INT end, INT row)
{
    DWORD *and, *xor, brush_row = row % pdev->brush_dib.height;

    if(!pdev->brush_and_bits)
        generate_masks(pdev, &pdev->brush_dib,
                       &pdev->brush_and_bits, &pdev->brush_xor_bits);

    order_end_points(&start, &end);
    and = (DWORD *)((char *)pdev->brush_and_bits + brush_row * pdev->brush_dib.stride);
    xor = (DWORD *)((char *)pdev->brush_xor_bits + brush_row * pdev->brush_dib.stride);

    pdev->dib.funcs->pattern_hline(&pdev->dib, start, end, row, and, xor, pdev->brush_dib.width, start % pdev->brush_dib.width);
}

/***********************************************************************
 *           SelectBrush
 */
HBRUSH DIBENG_SelectBrush( dibeng_pdevice_t *pdev, HBRUSH hbrush )
{
    LOGBRUSH logbrush;

    GetObjectW(hbrush, sizeof(logbrush), &logbrush);

    HeapFree(GetProcessHeap(), 0, pdev->brush_dib.bits);
    HeapFree(GetProcessHeap(), 0, pdev->brush_dib.color_table);
    HeapFree(GetProcessHeap(), 0, pdev->brush_and_bits);
    HeapFree(GetProcessHeap(), 0, pdev->brush_xor_bits);
    pdev->brush_dib.bits = NULL;
    pdev->brush_dib.color_table = NULL;
    pdev->brush_and_bits = NULL;
    pdev->brush_xor_bits = NULL;

    switch (logbrush.lbStyle)
    {
    default:
        FIXME("Unhandled brush style %d\n", logbrush.lbStyle);
        /* fall through */
    case BS_SOLID:
        pdev->brush_hline = solid_brush_hline;
        pdev->brush_colorref = logbrush.lbColor;
        if(pdev->dib.bit_count == 1)
            fixup_fg_colors_1(pdev);
        else
            pdev->brush_color = pdev->dib.funcs->colorref_to_pixel(&pdev->dib, logbrush.lbColor);

        calc_and_xor_masks(GetROP2(pdev->hdc), pdev->brush_color,
                           &pdev->brush_and, &pdev->brush_xor);
        break;

    case BS_DIBPATTERN:
    {
        dib_info_t src;
        BITMAPINFO *bmi = GlobalLock16(logbrush.lbHatch);

        init_dib_from_packed(&src, bmi);
        copy_dib_color_info(&pdev->dib, &pdev->brush_dib);
        convert_dib(&src, &pdev->brush_dib);

        GlobalUnlock16(logbrush.lbHatch);
        pdev->brush_hline = pattern_brush_hline;
        break;
    }

    }
    return hbrush;
}

/***********************************************************************
 *           SetROP2
 */
INT DIBENG_SetROP2( dibeng_pdevice_t *pdev, INT rop )
{
    INT ret = GetROP2(pdev->hdc);

    if(ret != rop)
    {
        calc_and_xor_masks(rop, pdev->pen_color,   &pdev->pen_and,   &pdev->pen_xor);
        calc_and_xor_masks(rop, pdev->brush_color, &pdev->brush_and, &pdev->brush_xor);
        calc_and_xor_masks(rop, pdev->bkgnd_color, &pdev->bkgnd_and, &pdev->bkgnd_xor);
        HeapFree(GetProcessHeap(), 0, pdev->brush_and_bits);
        HeapFree(GetProcessHeap(), 0, pdev->brush_xor_bits);
        pdev->brush_and_bits = NULL;
        pdev->brush_xor_bits = NULL;
    }
    return ret;
}

/***********************************************************************
 *           SetBkColor
 */
COLORREF DIBENG_SetBkColor( dibeng_pdevice_t *pdev, COLORREF color )
{
    INT rop = GetROP2(pdev->hdc);

    /* HACK - we can't get the dib color table during SelectBitmap since it hasn't
       been initialized yet.  This is called from DC_InitDC so it's a convenient place
       to grab the color table. */
    if(pdev->dib.color_table_size && !pdev->dib.color_table)
    {
        pdev->dib.color_table = HeapAlloc(GetProcessHeap(), 0, sizeof(pdev->dib.color_table[0]) * pdev->dib.color_table_size);
        GetDIBColorTable(pdev->hdc, 0, pdev->dib.color_table_size, pdev->dib.color_table);
    }

    pdev->bkgnd_color = pdev->dib.funcs->colorref_to_pixel(&pdev->dib, color);

    if(pdev->dib.bit_count == 1)
        fixup_fg_colors_1(pdev);

    calc_and_xor_masks(rop, pdev->bkgnd_color, &pdev->bkgnd_and, &pdev->bkgnd_xor);

    return color;
}
