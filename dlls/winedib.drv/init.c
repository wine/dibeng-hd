/*
 * DIB Engine Initialization
 *
 * Copyright 2008 Huw Davies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wingdi.h"

#include "dibeng.h"

#include "wine/debug.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibeng);


/***********************************************************************
 *            DllMain
 */
BOOL WINAPI DllMain( HINSTANCE hinst, DWORD reason, LPVOID reserved )
{
    switch(reason)
    {
    case DLL_PROCESS_ATTACH:
        break;
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

/****************************************************************************
 *       CreateBitmap   (WINEDIB.DRV.@)
 *
 * Returns TRUE on success else FALSE
 */
BOOL DIBENG_CreateBitmap( dibeng_pdevice_t *pdev, HBITMAP hbitmap, LPVOID bmBits )
{
    TRACE("%p\n", hbitmap);

    return TRUE;
}

static void calc_shift_and_len(DWORD mask, int *shift, int *len)
{
    int s, l;

    s = 0;
    while ((mask & 1) == 0)
    {
        mask >>= 1;
        s++;
    }
    l = 0;
    while ((mask & 1) == 1)
    {
        mask >>= 1;
        l++;
    }
    *shift = s;
    *len = l;
}

static void init_bit_fields(dib_info_t *dib, const DWORD *bit_fields)
{
    dib->red_mask    = bit_fields[0];
    dib->green_mask  = bit_fields[1];
    dib->blue_mask   = bit_fields[2];
    calc_shift_and_len(dib->red_mask,   &dib->red_shift,   &dib->red_len);
    calc_shift_and_len(dib->green_mask, &dib->green_shift, &dib->green_len);
    calc_shift_and_len(dib->blue_mask,  &dib->blue_shift,  &dib->blue_len);
}

static BOOL init_dib(dib_info_t *dib, const BITMAPINFOHEADER *bi, const DWORD *bit_fields,
                     const RGBQUAD *color_table, void *bits)
{
    dib->bit_count = bi->biBitCount;
    dib->width     = bi->biWidth;
    dib->height    = bi->biHeight;
    dib->stride    = ((dib->width * dib->bit_count + 31) >> 3) & ~3;

    dib->bits      = bits;

    dib->color_table_size = 0;
    dib->color_table = NULL;

    if(dib->height < 0) /* top-down */
    {
        dib->height = -dib->height;
    }
    else /* bottom-up */
    {
        /* data->bits always points to the top-left corner and the stride is -ve */
        dib->bits    = (BYTE*)dib->bits + (dib->height - 1) * dib->stride;
        dib->stride  = -dib->stride;
    }

    switch(dib->bit_count)
    {
    case 24:
        dib->funcs = &funcs_888;
        break;

    case 32:
        init_bit_fields(dib, bit_fields);

        if(dib->red_mask == 0xff0000 && dib->green_mask == 0x00ff00 && dib->blue_mask == 0x0000ff)
            dib->funcs = &funcs_8888;
        else
            dib->funcs = &funcs_32;
        break;

    case 16:
        init_bit_fields(dib, bit_fields);

        if(dib->red_mask == 0x7c00 && dib->green_mask == 0x03e0 && dib->blue_mask == 0x001f)
            dib->funcs = &funcs_555;
        else if(dib->red_mask == 0xf800 && dib->green_mask == 0x07e0 && dib->blue_mask == 0x001f)
            dib->funcs = &funcs_565;
        else
            dib->funcs = &funcs_16;
        break;

    case 8:
        dib->funcs = &funcs_8;
        dib->color_table_size = 256;
        if(bi->biClrUsed) dib->color_table_size = bi->biClrUsed;
        break;

    case 4:
        dib->funcs = &funcs_4;
        dib->color_table_size = 16;
        if(bi->biClrUsed) dib->color_table_size = bi->biClrUsed;
        break;

    case 1:
        dib->funcs = &funcs_1;
        dib->color_table_size = 2;
        if(bi->biClrUsed) dib->color_table_size = bi->biClrUsed;
        break;

    default:
        dib->funcs = NULL;
        FIXME("bpp %d not supported\n", dib->bit_count);
        return FALSE;
    }

    if(color_table)
    {
        DWORD size = dib->color_table_size * sizeof(dib->color_table[0]);
        dib->color_table = HeapAlloc(GetProcessHeap(), 0, size);
        memcpy(dib->color_table, color_table, size);
    }
    return TRUE;
}

BOOL init_dib_from_packed(dib_info_t *dib, void *packed)
{
    static const DWORD bit_fields_8888[3] = {0xff0000, 0x00ff00, 0x0000ff};
    static const DWORD bit_fields_555[3] = {0x7c00, 0x03e0, 0x001f};
    BITMAPINFOHEADER *bi = packed;
    const DWORD *masks = NULL;
    RGBQUAD *color_table = NULL;
    BYTE *ptr = (BYTE*)packed + bi->biSize;
    int num_colors = bi->biClrUsed;

    if(bi->biCompression == BI_BITFIELDS)
    {
        masks = (DWORD *)ptr;
        ptr += 3 * sizeof(DWORD);
    }
    else if(bi->biBitCount == 32)
        masks = bit_fields_8888;
    else if(bi->biBitCount == 16)
        masks = bit_fields_555;

    if(!num_colors && bi->biBitCount <= 8) num_colors = 1 << bi->biBitCount;
    if(num_colors) color_table = (RGBQUAD*)ptr;
    ptr += num_colors * sizeof(*color_table);

    return init_dib(dib, bi, masks, color_table, ptr);
}

void copy_dib_color_info(const dib_info_t *src, dib_info_t *dst)
{
    dst->bit_count        = src->bit_count;
    dst->red_mask         = src->red_mask;
    dst->green_mask       = src->green_mask;
    dst->blue_mask        = src->blue_mask;
    dst->red_len          = src->red_len;
    dst->green_len        = src->green_len;
    dst->blue_len         = src->blue_len;
    dst->red_shift        = src->red_shift;
    dst->green_shift      = src->green_shift;
    dst->blue_shift       = src->blue_shift;
    dst->funcs            = src->funcs;
    dst->color_table_size = src->color_table_size;
    dst->color_table      = NULL;
    if(dst->color_table_size)
    {
        int size = dst->color_table_size * sizeof(dst->color_table[0]);
        dst->color_table = HeapAlloc(GetProcessHeap(), 0, dst->color_table_size * sizeof(dst->color_table[0]));
        memcpy(dst->color_table, src->color_table, size);
    }
}

static BOOL dib_formats_match(const dib_info_t *d1, const dib_info_t *d2)
{
    if(d1->bit_count != d2->bit_count) return FALSE;

    switch(d1->bit_count)
    {
    case 24: return TRUE;

    case 32:
    case 16:
        return (d1->red_mask == d2->red_mask) && (d1->green_mask == d2->green_mask) &&
            (d1->blue_mask == d2->blue_mask);

    case 8:
    case 4:
    case 1:
        if(d1->color_table_size != d2->color_table_size) return FALSE;

        return !memcmp(d1->color_table, d2->color_table, d1->color_table_size * sizeof(d1->color_table[0]));

    default:
        ERR("Unexpected depth %d\n", d1->bit_count);
        return FALSE;
    }
}

/* convert a given dib into another format.
   We could spilt this into two and have convert_to_8888 and convert_from_8888
   as functions in the primitive function table */
void convert_dib(const dib_info_t *src, dib_info_t *dst)
{
    DWORD color, pixel;
    COLORREF color_ref;
    int x, y;

    dst->height = src->height;
    dst->width = src->width;
    dst->stride = ((dst->width * dst->bit_count + 31) >> 3) & ~3;
    dst->bits = HeapAlloc(GetProcessHeap(), 0, dst->height * dst->stride);

    if(dib_formats_match(src, dst))
    {
        if(src->stride > 0)
            memcpy(dst->bits, src->bits, dst->height * dst->stride);
        else
        {
            BYTE *src_bits = src->bits;
            BYTE *dst_bits = dst->bits;
            for(y = 0; y < dst->height; y++)
            {
                memcpy(dst_bits, src_bits, dst->stride);
                dst_bits += dst->stride;
                src_bits += src->stride;
            }
        }
        return;
    }

    for(y = 0; y < src->height; y++)
    {
        for(x = 0; x < src->width; x++)
        {
            color = src->funcs->get_pixel_rgb(src, x, y);
            color_ref = RGB((color >> 16), (color >> 8), color);
            pixel = dst->funcs->colorref_to_pixel(dst, color_ref);
            dst->funcs->set_pixel(dst, x, y, 0, pixel);
        }
    }
    return;
}

/****************************************************************************
 *       SelectBitmap   (WINEDIB.DRV.@)
 */
HBITMAP DIBENG_SelectBitmap( dibeng_pdevice_t *pdev, HBITMAP hbitmap )
{
    DIBSECTION ds;

    if(GetObjectW(hbitmap, sizeof(ds), &ds) == sizeof(BITMAP))
    {
        /* Stock bitmap */
        ds.dsBmih.biWidth = 1;
        ds.dsBmih.biHeight = 1;
        ds.dsBmih.biBitCount = 1;
        ds.dsBmih.biClrUsed = 0;
        ds.dsBm.bmBits = NULL;
    }

    HeapFree(GetProcessHeap(), 0, pdev->dib.color_table);
    pdev->dib.color_table = NULL;

    init_dib(&pdev->dib, &ds.dsBmih, ds.dsBitfields, NULL, ds.dsBm.bmBits);

    return hbitmap;
}

static inline void clear_dib_info(dib_info_t *dib)
{
    dib->bits = dib->color_table = NULL;
}

/****************************************************************************
 *       CreateDC   (WINEDIB.DRV.@)
 */
BOOL DIBENG_CreateDC( HDC hdc, dibeng_pdevice_t **pdev, LPCWSTR driver, LPCWSTR device,
                      LPCWSTR output, const DEVMODEW* initData )
{
    dibeng_pdevice_t *physDev;

    physDev = HeapAlloc( GetProcessHeap(), 0, sizeof(*physDev) );
    if (!physDev) return FALSE;

    *pdev = physDev;
    physDev->hdc = hdc;
    clear_dib_info(&physDev->dib);
    clear_dib_info(&physDev->brush_dib);
    physDev->brush_and_bits = NULL;
    physDev->brush_xor_bits = NULL;

    return TRUE;
}
