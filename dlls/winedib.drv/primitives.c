/*
 * DIB Engine Primitives
 *
 * Copyright 2008 Huw Davies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wingdi.h"

#include "dibeng.h"

#include "wine/debug.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibeng);

static inline void do_rop_32(DWORD *ptr, DWORD and, DWORD xor)
{
    *ptr = (*ptr & and) ^ xor;
}

static inline void do_rop_16(WORD *ptr, WORD and, WORD xor)
{
    *ptr = (*ptr & and) ^ xor;
}

static inline void do_rop_8(BYTE *ptr, BYTE and, BYTE xor)
{
    *ptr = (*ptr & and) ^ xor;
}

static void solid_hline_32(const dib_info_t *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    DWORD *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    for(i = start; i < end; i++)
        do_rop_32(ptr++, and, xor);
}

static void solid_hline_24(const dib_info_t *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE and_bytes[3], xor_bytes[3];

    and_bytes[0] =  and        & 0xff;
    and_bytes[1] = (and >> 8)  & 0xff;
    and_bytes[2] = (and >> 16) & 0xff;
    xor_bytes[0] =  xor        & 0xff;
    xor_bytes[1] = (xor >> 8)  & 0xff;
    xor_bytes[2] = (xor >> 16) & 0xff;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr++, and_bytes[0], xor_bytes[0]);
        do_rop_8(ptr++, and_bytes[1], xor_bytes[1]);
        do_rop_8(ptr++, and_bytes[2], xor_bytes[2]);
    }
}

static void solid_hline_16(const dib_info_t *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    WORD *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    for(i = start; i < end; i++)
        do_rop_16(ptr++, and, xor);
}

static void solid_hline_8(const dib_info_t *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    for(i = start; i < end; i++)
        do_rop_8(ptr++, and, xor);
}

static void solid_hline_4(const dib_info_t *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE byte_and, byte_xor;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);
    byte_and = (and & 0xf) | ((and << 4) & 0xf0);
    byte_xor = (xor & 0xf) | ((xor << 4) & 0xf0);

    if(start & 1) /* upper nibble untouched */
        do_rop_8(ptr++, byte_and | 0xf0, byte_xor & 0x0f);

    for(i = (start + 1) / 2; i < end / 2; i++)
        do_rop_8(ptr++, byte_and, byte_xor);

    if(end & 1) /* lower nibble untouched */
        do_rop_8(ptr, byte_and | 0x0f, byte_xor & 0xf0);
}

static void solid_hline_1(const dib_info_t *dib, int start, int end, int row, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE byte_and = 0, byte_xor = 0, mask;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    if(and & 1) byte_and = 0xff;
    if(xor & 1) byte_xor = 0xff;

    if((start & ~7) == (end & ~7)) /* special case run inside one byte */
    {
        mask = ((1L << ((end & 7) - (start & 7))) - 1) << (8 - (end & 7));
        do_rop_8(ptr, byte_and | ~mask, byte_xor & mask);
        return;
    }

    if(start & 7)
    {
        mask = (1 << (8 - (start & 7))) - 1;
        do_rop_8(ptr++, byte_and | ~mask, byte_xor & mask);
    }

    for(i = (start + 7) / 8; i < end / 8; i++)
        do_rop_8(ptr++, byte_and, byte_xor);

    if(end & 7)
    {
        mask = ~((1 << (8 - (end & 7))) - 1);
        do_rop_8(ptr++, byte_and | ~mask, byte_xor & mask);
    }
}

static void pattern_hline_32(const dib_info_t *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    DWORD *ptr;
    const DWORD *and_ptr = and, *xor_ptr = xor;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset;
    xor_ptr += offset;
    for(i = start; i < end; i++)
    {
        do_rop_32(ptr++, *and_ptr++, *xor_ptr++);
        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
    }
}

static void pattern_hline_24(const dib_info_t *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    BYTE *ptr;
    const BYTE *and_ptr = and, *xor_ptr = xor;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset * 3;
    xor_ptr += offset * 3;

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr++,  *and_ptr++, *xor_ptr++);
        do_rop_8(ptr++,  *and_ptr++, *xor_ptr++);
        do_rop_8(ptr++,  *and_ptr++, *xor_ptr++);
        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
    }
}

static void pattern_hline_16(const dib_info_t *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    WORD *ptr;
    const WORD *and_ptr = and, *xor_ptr = xor;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset;
    xor_ptr += offset;

    for(i = start; i < end; i++)
    {
        do_rop_16(ptr++, *and_ptr++, *xor_ptr++);
        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
    }
}

static void pattern_hline_8(const dib_info_t *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    BYTE *ptr;
    const BYTE *and_ptr = and, *xor_ptr = xor;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset;
    xor_ptr += offset;

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr++, *and_ptr++, *xor_ptr++);
        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
    }
}

static void pattern_hline_4(const dib_info_t *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    BYTE *ptr;
    const BYTE *and_ptr = and, *xor_ptr = xor;
    int i;
    BYTE byte_and, byte_xor;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset / 2;
    xor_ptr += offset / 2;

    for(i = start; i < end; i++)
    {
        if(offset & 1)
        {
            byte_and = *and_ptr++ & 0x0f;
            byte_xor = *xor_ptr++ & 0x0f;
        }
        else
        {
            byte_and = (*and_ptr & 0xf0) >> 4;
            byte_xor = (*xor_ptr & 0xf0) >> 4;
        }

        if(i & 1)
            byte_and |= 0xf0;
        else
        {
            byte_and = (byte_and << 4) | 0x0f;
            byte_xor <<= 4;
        }

        do_rop_8(ptr, byte_and, byte_xor);

        if(i & 1) ptr++;

        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
    }
}

static void pattern_hline_1(const dib_info_t *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset)
{
    BYTE *ptr;
    const BYTE *and_ptr = and, *xor_ptr = xor;
    int i;
    BYTE byte_and, byte_xor, dst_mask, brush_mask;

    ptr = dib->funcs->get_pixel_ptr(dib, start, row);

    and_ptr += offset / 8;
    xor_ptr += offset / 8;

    for(i = start; i < end; i++)
    {
        dst_mask   = 1 << (7 - (i      & 7));
        brush_mask = 1 << (7 - (offset & 7));

        byte_and = (*and_ptr & brush_mask) ? 0xff : 0;
        byte_xor = (*xor_ptr & brush_mask) ? 0xff : 0;

        byte_and |= ~dst_mask;
        byte_xor &= dst_mask;

        do_rop_8(ptr, byte_and, byte_xor);

        if((i & 7) == 7) ptr++;
        if(++offset == count)
        {
            offset = 0;
            and_ptr = and;
            xor_ptr = xor;
        }
        else if((offset & 7) == 7)
        {
            and_ptr++;
            xor_ptr++;
        }
    }
}

static void solid_vline_32(const dib_info_t *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_32((DWORD*)ptr, and, xor);
        ptr += dib->stride;
    }
}

static void solid_vline_24(const dib_info_t *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE and_bytes[3], xor_bytes[3];

    and_bytes[0] =  and        & 0xff;
    and_bytes[1] = (and >> 8)  & 0xff;
    and_bytes[2] = (and >> 16) & 0xff;
    xor_bytes[0] =  xor        & 0xff;
    xor_bytes[1] = (xor >> 8)  & 0xff;
    xor_bytes[2] = (xor >> 16) & 0xff;

    ptr  = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr, and_bytes[0], xor_bytes[0]);
        do_rop_8(ptr + 1, and_bytes[1], xor_bytes[1]);
        do_rop_8(ptr + 2, and_bytes[2], xor_bytes[2]);
        ptr += dib->stride;
    }
}

static void solid_vline_16(const dib_info_t *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_16((WORD*)ptr, and, xor);
        ptr += dib->stride;
    }
}

static void solid_vline_8(const dib_info_t *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;

    ptr = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr, and, xor);
        ptr += dib->stride;
    }
}

static void solid_vline_4(const dib_info_t *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE byte_and, byte_xor;

    if(col & 1) /* upper nibble untouched */
    {
        byte_and = (and & 0xf) | 0xf0;
        byte_xor = (xor & 0xf);
    }
    else
    {
        byte_and = ((and << 4) & 0xf0) | 0x0f;
        byte_xor = ((xor << 4) & 0xf0);
    }

    ptr = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr, byte_and, byte_xor);
        ptr += dib->stride;
    }
}

static void solid_vline_1(const dib_info_t *dib, int col, int start, int end, DWORD and, DWORD xor)
{
    BYTE *ptr;
    int i;
    BYTE byte_and = 0, byte_xor = 0, mask;

    if(and & 1) byte_and = 0xff;
    if(xor & 1) byte_xor = 0xff;

    mask = 1 << (7 - (col & 7));

    byte_and |= ~mask;
    byte_xor &= mask;

    ptr = dib->funcs->get_pixel_ptr(dib, col, start);

    for(i = start; i < end; i++)
    {
        do_rop_8(ptr, byte_and, byte_xor);
        ptr += dib->stride;
    }
}

static void *get_pixel_ptr_32(const dib_info_t *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x * 4;
    return ptr;
}

static void *get_pixel_ptr_24(const dib_info_t *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x * 3;
    return ptr;
}

static void *get_pixel_ptr_16(const dib_info_t *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x * 2;
    return ptr;
}

static void *get_pixel_ptr_8(const dib_info_t *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x;
    return ptr;
}

static void *get_pixel_ptr_4(const dib_info_t *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x / 2;
    return ptr;
}

static void *get_pixel_ptr_1(const dib_info_t *dib, int x, int y)
{
    BYTE *ptr = dib->bits;

    ptr += (y * dib->stride);

    ptr += x / 8;
    return ptr;
}

static void set_pixel_32(const dib_info_t *dib, int x, int y, DWORD and, DWORD xor)
{
    DWORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    do_rop_32(ptr, and, xor);
}

static void set_pixel_24(const dib_info_t *dib, int x, int y, DWORD and, DWORD xor)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    do_rop_8(ptr,      and        & 0xff,  xor        & 0xff);
    do_rop_8(ptr + 1, (and >> 8)  & 0xff, (xor >> 8)  & 0xff);
    do_rop_8(ptr + 2, (and >> 16) & 0xff, (xor >> 16) & 0xff);
}

static void set_pixel_16(const dib_info_t *dib, int x, int y, DWORD and, DWORD xor)
{
    WORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    do_rop_16(ptr, and, xor);
}

static void set_pixel_8(const dib_info_t *dib, int x, int y, DWORD and, DWORD xor)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    do_rop_8(ptr, and, xor);
}

static void set_pixel_4(const dib_info_t *dib, int x, int y, DWORD and, DWORD xor)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    BYTE byte_and, byte_xor;

    if(x & 1) /* upper nibble untouched */
    {
        byte_and = (and & 0xf) | 0xf0;
        byte_xor = (xor & 0xf);
    }
    else
    {
        byte_and = ((and << 4) & 0xf0) | 0x0f;
        byte_xor = ((xor << 4) & 0xf0);
    }

    do_rop_8(ptr, byte_and, byte_xor);
}

static void set_pixel_1(const dib_info_t *dib, int x, int y, DWORD and, DWORD xor)
{
    BYTE *ptr;
    BYTE byte_and = 0, byte_xor = 0, mask;

    if(and & 1) byte_and = 0xff;
    if(xor & 1) byte_xor = 0xff;

    mask = 1 << (7 - (x & 7));

    byte_and |= ~mask;
    byte_xor &= mask;

    ptr = dib->funcs->get_pixel_ptr(dib, x, y);

    do_rop_8(ptr, byte_and, byte_xor);
}

static DWORD get_pixel_rgb_8888(const dib_info_t *dib, int x, int y)
{
    DWORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    return *ptr;
}

static DWORD get_field (DWORD pixel, int shift, int len)
{
    pixel = pixel & (((1 << (len)) - 1) << shift);
    pixel = pixel << (32 - (shift + len)) >> 24;
    return pixel;
}

static DWORD get_pixel_rgb_masks(const dib_info_t *dib, int x, int y)
{
    DWORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);

    return get_field(*ptr, dib->red_shift,   dib->red_len)   << 16 |
           get_field(*ptr, dib->green_shift, dib->green_len) <<  8 |
           get_field(*ptr, dib->blue_shift,  dib->blue_len);
}

static DWORD get_pixel_rgb_888(const dib_info_t *dib, int x, int y)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    return (ptr[0] << 16) | (ptr[1] << 8) | ptr[2];
}

static DWORD get_pixel_rgb_555(const dib_info_t *dib, int x, int y)
{
    WORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    return ((*ptr & 0x7c00) << 9) | ((*ptr & 0x03e0) << 6) | ((*ptr & 0x001f) << 3);
}

static DWORD get_pixel_rgb_565(const dib_info_t *dib, int x, int y)
{
    WORD *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    return ((*ptr & 0xf800) << 8) | ((*ptr & 0x07e0) << 5) | ((*ptr & 0x001f) << 3);
}

static DWORD get_pixel_rgb_8(const dib_info_t *dib, int x, int y)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y);
    RGBQUAD *color = dib->color_table + *ptr;
    return (color->rgbRed << 16) | (color->rgbGreen << 8) | color->rgbBlue;
}

static DWORD get_pixel_rgb_4(const dib_info_t *dib, int x, int y)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y), pix;
    RGBQUAD *color;

    if(x & 1)
        pix = *ptr & 0x0f;
    else
        pix = *ptr >> 4;

    color = dib->color_table + pix;
    return (color->rgbRed << 16) | (color->rgbGreen << 8) | color->rgbBlue;
}

static DWORD get_pixel_rgb_1(const dib_info_t *dib, int x, int y)
{
    BYTE *ptr = dib->funcs->get_pixel_ptr(dib, x, y), pix;
    RGBQUAD *color;

    pix = *ptr;

    pix >>= (7 - (x & 7));
    pix &= 1;

    color = dib->color_table + pix;
    return (color->rgbRed << 16) | (color->rgbGreen << 8) | color->rgbBlue;
}

static DWORD colorref_to_pixel_888(const dib_info_t *dib, COLORREF color)
{
    return ( ((color >> 16) & 0xff) | (color & 0xff00) | ((color << 16) & 0xff0000) );
}

static inline DWORD put_field(DWORD field, int shift, int len)
{
    shift = shift - (8 - len);
    if (len <= 8)
        field &= (((1 << len) - 1) << (8 - len));
    if (shift < 0)
        field >>= -shift;
    else
        field <<= shift;
    return field;
}

static DWORD colorref_to_pixel_masks(const dib_info_t *dib, COLORREF color)
{
    DWORD r,g,b;

    r = GetRValue(color);
    g = GetGValue(color);
    b = GetBValue(color);

    return put_field(r, dib->red_shift,   dib->red_len) |
           put_field(g, dib->green_shift, dib->green_len) |
           put_field(b, dib->blue_shift,  dib->blue_len);
}

static DWORD colorref_to_pixel_555(const dib_info_t *dib, COLORREF color)
{
    return ( ((color >> 19) & 0x001f) | ((color >> 6) & 0x03e0) | ((color << 7) & 0x7c00) );
}

static DWORD colorref_to_pixel_565(const dib_info_t *dib, COLORREF color)
{
    return ( ((color >> 19) & 0x001f) | ((color >> 5) & 0x07e0) | ((color << 8) & 0xf800) );
}

static DWORD colorref_to_pixel_colortable(const dib_info_t *dib, COLORREF color)
{
    int i, best_index = 0;
    RGBQUAD rgb;
    DWORD diff, best_diff = 0xffffffff;

    rgb.rgbRed = GetRValue(color);
    rgb.rgbGreen = GetGValue(color);
    rgb.rgbBlue = GetBValue(color);

    for(i = 0; i < dib->color_table_size; i++)
    {
        RGBQUAD *cur = dib->color_table + i;
        diff = (rgb.rgbRed - cur->rgbRed) * (rgb.rgbRed - cur->rgbRed)
            +  (rgb.rgbGreen - cur->rgbGreen) * (rgb.rgbGreen - cur->rgbGreen)
            +  (rgb.rgbBlue - cur->rgbBlue) * (rgb.rgbBlue - cur->rgbBlue);

        if(diff == 0)
        {
            best_index = i;
            break;
        }

        if(diff < best_diff)
        {
            best_diff = diff;
            best_index = i;
        }
    }
    return best_index;
}

primitive_funcs_t funcs_8888 =
{
    solid_hline_32,
    pattern_hline_32,
    solid_vline_32,
    get_pixel_ptr_32,
    set_pixel_32,
    get_pixel_rgb_8888,
    colorref_to_pixel_888
};

primitive_funcs_t funcs_32 =
{
    solid_hline_32,
    pattern_hline_32,
    solid_vline_32,
    get_pixel_ptr_32,
    set_pixel_32,
    get_pixel_rgb_masks,
    colorref_to_pixel_masks
};

primitive_funcs_t funcs_888 =
{
    solid_hline_24,
    pattern_hline_24,
    solid_vline_24,
    get_pixel_ptr_24,
    set_pixel_24,
    get_pixel_rgb_888,
    colorref_to_pixel_888
};

primitive_funcs_t funcs_555 =
{
    solid_hline_16,
    pattern_hline_16,
    solid_vline_16,
    get_pixel_ptr_16,
    set_pixel_16,
    get_pixel_rgb_555,
    colorref_to_pixel_555
};

primitive_funcs_t funcs_565 =
{
    solid_hline_16,
    pattern_hline_16,
    solid_vline_16,
    get_pixel_ptr_16,
    set_pixel_16,
    get_pixel_rgb_565,
    colorref_to_pixel_565
};

primitive_funcs_t funcs_16 =
{
    solid_hline_16,
    pattern_hline_16,
    solid_vline_16,
    get_pixel_ptr_16,
    set_pixel_16,
    get_pixel_rgb_masks,
    colorref_to_pixel_masks
};

primitive_funcs_t funcs_8 =
{
    solid_hline_8,
    pattern_hline_8,
    solid_vline_8,
    get_pixel_ptr_8,
    set_pixel_8,
    get_pixel_rgb_8,
    colorref_to_pixel_colortable
};

primitive_funcs_t funcs_4 =
{
    solid_hline_4,
    pattern_hline_4,
    solid_vline_4,
    get_pixel_ptr_4,
    set_pixel_4,
    get_pixel_rgb_4,
    colorref_to_pixel_colortable
};

primitive_funcs_t funcs_1 =
{
    solid_hline_1,
    pattern_hline_1,
    solid_vline_1,
    get_pixel_ptr_1,
    set_pixel_1,
    get_pixel_rgb_1,
    colorref_to_pixel_colortable
};
