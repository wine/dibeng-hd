/*
 * DIB Engine Tests
 *
 * Copyright 2008 Huw Davies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>

#include "windef.h"
#include "winbase.h"
#include "wingdi.h"
#include "winuser.h"
#include "wincrypt.h"

#include "wine/test.h"

static HCRYPTPROV crypt_prov;

static const DWORD rop3[256] =
{
    0x00000042, /* 0 BLACKNESS */
    0x00010289, /* DPSoon */
    0x00020C89, /* DPSona */
    0x000300AA, /* PSon */
    0x00040C88, /* SDPona */
    0x000500A9, /* DPon */
    0x00060865, /* PDSxnon */
    0x000702C5, /* PDSaon */
    0x00080F08, /* SDPnaa */
    0x00090245, /* PDSxon */
    0x000A0329, /* DPna */
    0x000B0B2A, /* PSDnaon */
    0x000C0324, /* SPna */
    0x000D0B25, /* PDSnaon */
    0x000E08A5, /* PDSonon */
    0x000F0001, /* Pn */
    0x00100C85, /* PDSona */
    0x001100A6, /* DSon NOTSRCERASE */
    0x00120868, /* SDPxnon */
    0x001302C8, /* SDPaon */
    0x00140869, /* DPSxnon */
    0x001502C9, /* DPSaon */
    0x00165CCA, /* PSDPSanaxx */
    0x00171D54, /* SSPxDSxaxn */
    0x00180D59, /* SPxPDxa */
    0x00191CC8, /* SDPSanaxn */
    0x001A06C5, /* PDSPaox */
    0x001B0768, /* SDPSxaxn */
    0x001C06CA, /* PSDPaox */
    0x001D0766, /* DSPDxaxn */
    0x001E01A5, /* PDSox */
    0x001F0385, /* PDSoan */
    0x00200F09, /* DPSnaa */
    0x00210248, /* SDPxon */
    0x00220326, /* DSna */
    0x00230B24, /* SPDnaon */
    0x00240D55, /* SPxDSxa */
    0x00251CC5, /* PDSPanaxn */
    0x002606C8, /* SDPSaox */
    0x00271868, /* SDPSxnox */
    0x00280369, /* DPSxa */
    0x002916CA, /* PSDPSaoxxn */
    0x002A0CC9, /* DPSana */
    0x002B1D58, /* SSPxPDxaxn */
    0x002C0784, /* SPDSoax */
    0x002D060A, /* PSDnox */
    0x002E064A, /* PSDPxox */
    0x002F0E2A, /* PSDnoan */
    0x0030032A, /* PSna */
    0x00310B28, /* SDPnaon */
    0x00320688, /* SDPSoox */
    0x00330008, /* Sn NOTSRCCOPY */
    0x003406C4, /* SPDSaox */
    0x00351864, /* SPDSxnox */
    0x003601A8, /* SDPox */
    0x00370388, /* SDPoan */
    0x0038078A, /* PSDPoax */
    0x00390604, /* SPDnox */
    0x003A0644, /* SPDSxox */
    0x003B0E24, /* SPDnoan */
    0x003C004A, /* PSx */
    0x003D18A4, /* SPDSonox */
    0x003E1B24, /* SPDSnaox */
    0x003F00EA, /* PSan */
    0x00400F0A, /* PSDnaa */
    0x00410249, /* DPSxon */
    0x00420D5D, /* SDxPDxa */
    0x00431CC4, /* SPDSanaxn */
    0x00440328, /* SDna SRCERASE */
    0x00450B29, /* DPSnaon */
    0x004606C6, /* DSPDaox */
    0x0047076A, /* PSDPxaxn */
    0x00480368, /* SDPxa */
    0x004916C5, /* PDSPDaoxxn */
    0x004A0789, /* DPSDoax */
    0x004B0605, /* PDSnox */
    0x004C0CC8, /* SDPana */
    0x004D1954, /* SSPxDSxoxn */
    0x004E0645, /* PDSPxox */
    0x004F0E25, /* PDSnoan */
    0x00500325, /* PDna */
    0x00510B26, /* DSPnaon */
    0x005206C9, /* DPSDaox */
    0x00530764, /* SPDSxaxn */
    0x005408A9, /* DPSonon */
    0x00550009, /* Dn DSTINVERT */
    0x005601A9, /* DPSox */
    0x00570389, /* DPSoan */
    0x00580785, /* PDSPoax */
    0x00590609, /* DPSnox */
    0x005A0049, /* DPx PATINVERT */
    0x005B18A9, /* DPSDonox */
    0x005C0649, /* DPSDxox */
    0x005D0E29, /* DPSnoan */
    0x005E1B29, /* DPSDnaox */
    0x005F00E9, /* DPan */
    0x00600365, /* PDSxa */
    0x006116C6, /* DSPDSaoxxn */
    0x00620786, /* DSPDoax */
    0x00630608, /* SDPnox */
    0x00640788, /* SDPSoax */
    0x00650606, /* DSPnox */
    0x00660046, /* DSx SRCINVERT */
    0x006718A8, /* SDPSonox */
    0x006858A6, /* DSPDSonoxxn */
    0x00690145, /* PDSxxn */
    0x006A01E9, /* DPSax */
    0x006B178A, /* PSDPSoaxxn */
    0x006C01E8, /* SDPax */
    0x006D1785, /* PDSPDoaxxn */
    0x006E1E28, /* SDPSnoax */
    0x006F0C65, /* PDSxnan */
    0x00700CC5, /* PDSana */
    0x00711D5C, /* SSDxPDxaxn */
    0x00720648, /* SDPSxox */
    0x00730E28, /* SDPnoan */
    0x00740646, /* DSPDxox */
    0x00750E26, /* DSPnoan */
    0x00761B28, /* SDPSnaox */
    0x007700E6, /* DSan */
    0x007801E5, /* PDSax */
    0x00791786, /* DSPDSoaxxn */
    0x007A1E29, /* DPSDnoax */
    0x007B0C68, /* SDPxnan */
    0x007C1E24, /* SPDSnoax */
    0x007D0C69, /* DPSxnan */
    0x007E0955, /* SPxDSxo */
    0x007F03C9, /* DPSaan */
    0x008003E9, /* DPSaa */
    0x00810975, /* SPxDSxon */
    0x00820C49, /* DPSxna */
    0x00831E04, /* SPDSnoaxn */
    0x00840C48, /* SDPxna */
    0x00851E05, /* PDSPnoaxn */
    0x008617A6, /* DSPDSoaxx */
    0x008701C5, /* PDSaxn */
    0x008800C6, /* DSa SRCAND */
    0x00891B08, /* SDPSnaoxn */
    0x008A0E06, /* DSPnoa */
    0x008B0666, /* DSPDxoxn */
    0x008C0E08, /* SDPnoa */
    0x008D0668, /* SDPSxoxn */
    0x008E1D7C, /* SSDxPDxax */
    0x008F0CE5, /* PDSanan */
    0x00900C45, /* PDSxna */
    0x00911E08, /* SDPSnoaxn */
    0x009217A9, /* DPSDPoaxx */
    0x009301C4, /* SPDaxn */
    0x009417AA, /* PSDPSoaxx */
    0x009501C9, /* DPSaxn */
    0x00960169, /* DPSxx */
    0x0097588A, /* PSDPSonoxx */
    0x00981888, /* SDPSonoxn */
    0x00990066, /* DSxn */
    0x009A0709, /* DPSnax */
    0x009B07A8, /* SDPSoaxn */
    0x009C0704, /* SPDnax */
    0x009D07A6, /* DSPDoaxn */
    0x009E16E6, /* DSPDSaoxx */
    0x009F0345, /* PDSxan */
    0x00A000C9, /* DPa */
    0x00A11B05, /* PDSPnaoxn */
    0x00A20E09, /* DPSnoa */
    0x00A30669, /* DPSDxoxn */
    0x00A41885, /* PDSPonoxn */
    0x00A50065, /* PDxn */
    0x00A60706, /* DSPnax */
    0x00A707A5, /* PDSPoaxn */
    0x00A803A9, /* DPSoa */
    0x00A90189, /* DPSoxn */
    0x00AA0029, /* D */
    0x00AB0889, /* DPSono */
    0x00AC0744, /* SPDSxax */
    0x00AD06E9, /* DPSDaoxn */
    0x00AE0B06, /* DSPnao */
    0x00AF0229, /* DPno */
    0x00B00E05, /* PDSnoa */
    0x00B10665, /* PDSPxoxn */
    0x00B21974, /* SSPxDSxox */
    0x00B30CE8, /* SDPanan */
    0x00B4070A, /* PSDnax */
    0x00B507A9, /* DPSDoaxn */
    0x00B616E9, /* DPSDPaoxx */
    0x00B70348, /* SDPxan */
    0x00B8074A, /* PSDPxax */
    0x00B906E6, /* DSPDaoxn */
    0x00BA0B09, /* DPSnao */
    0x00BB0226, /* DSno MERGEPAINT */
    0x00BC1CE4, /* SPDSanax */
    0x00BD0D7D, /* SDxPDxan */
    0x00BE0269, /* DPSxo */
    0x00BF08C9, /* DPSano */
    0x00C000CA, /* PSa MERGECOPY */
    0x00C11B04, /* SPDSnaoxn */
    0x00C21884, /* SPDSonoxn */
    0x00C3006A, /* PSxn */
    0x00C40E04, /* SPDnoa */
    0x00C50664, /* SPDSxoxn */
    0x00C60708, /* SDPnax */
    0x00C707AA, /* PSDPoaxn */
    0x00C803A8, /* SDPoa */
    0x00C90184, /* SPDoxn */
    0x00CA0749, /* DPSDxax */
    0x00CB06E4, /* SPDSaoxn */
    0x00CC0020, /* S SRCCOPY */
    0x00CD0888, /* SDPono */
    0x00CE0B08, /* SDPnao */
    0x00CF0224, /* SPno */
    0x00D00E0A, /* PSDnoa */
    0x00D1066A, /* PSDPxoxn */
    0x00D20705, /* PDSnax */
    0x00D307A4, /* SPDSoaxn */
    0x00D41D78, /* SSPxPDxax */
    0x00D50CE9, /* DPSanan */
    0x00D616EA, /* PSDPSaoxx */
    0x00D70349, /* DPSxan */
    0x00D80745, /* PDSPxax */
    0x00D906E8, /* SDPSaoxn */
    0x00DA1CE9, /* DPSDanax */
    0x00DB0D75, /* SPxDSxan */
    0x00DC0B04, /* SPDnao */
    0x00DD0228, /* SDno */
    0x00DE0268, /* SDPxo */
    0x00DF08C8, /* SDPano */
    0x00E003A5, /* PDSoa */
    0x00E10185, /* PDSoxn */
    0x00E20746, /* DSPDxax */
    0x00E306EA, /* PSDPaoxn */
    0x00E40748, /* SDPSxax */
    0x00E506E5, /* PDSPaoxn */
    0x00E61CE8, /* SDPSanax */
    0x00E70D79, /* SPxPDxan */
    0x00E81D74, /* SSPxDSxax */
    0x00E95CE6, /* DSPDSanaxxn */
    0x00EA02E9, /* DPSao */
    0x00EB0849, /* DPSxno */
    0x00EC02E8, /* SDPao */
    0x00ED0848, /* SDPxno */
    0x00EE0086, /* DSo SRCPAINT */
    0x00EF0A08, /* SDPnoo */
    0x00F00021, /* P PATCOPY */
    0x00F10885, /* PDSono */
    0x00F20B05, /* PDSnao */
    0x00F3022A, /* PSno */
    0x00F40B0A, /* PSDnao */
    0x00F50225, /* PDno */
    0x00F60265, /* PDSxo */
    0x00F708C5, /* PDSano */
    0x00F802E5, /* PDSao */
    0x00F90845, /* PDSxno */
    0x00FA0089, /* DPo */
    0x00FB0A09, /* DPSnoo PATPAINT */
    0x00FC008A, /* PSo */
    0x00FD0A0A, /* PSDnoo */
    0x00FE02A9, /* DPSoo */
    0x00FF0062  /* 1 WHITENESS */
};

static inline DWORD get_stride(BITMAPINFO *bmi)
{
    return ((bmi->bmiHeader.biBitCount * bmi->bmiHeader.biWidth + 31) >> 3) & ~3;
}

static inline DWORD get_dib_size(BITMAPINFO *bmi)
{
    return get_stride(bmi) * abs(bmi->bmiHeader.biHeight);
}

static inline BOOL rop_uses_src(DWORD rop)
{
    return (((rop & 0xcc0000) >> 2) != (rop & 0x330000));
}

static inline BOOL rop_uses_pattern(DWORD rop)
{
    return (((rop & 0xf00000) >> 4) != (rop & 0x0f0000));
}

static char *hash_dib(BITMAPINFO *bmi, void *bits)
{
    DWORD dib_size = get_dib_size(bmi);
    HCRYPTHASH hash;
    char *buf;
    BYTE hash_buf[20];
    DWORD hash_size = sizeof(hash_buf);
    int i;
    static const char *hex = "0123456789abcdef";

    if(!crypt_prov) return NULL;

    if(!CryptCreateHash(crypt_prov, CALG_SHA1, 0, 0, &hash)) return NULL;

    CryptHashData(hash, bits, dib_size, 0);

    CryptGetHashParam(hash, HP_HASHVAL, NULL, &hash_size, 0);
    if(hash_size != sizeof(hash_buf)) return NULL;

    CryptGetHashParam(hash, HP_HASHVAL, hash_buf, &hash_size, 0);
    CryptDestroyHash(hash);

    buf = HeapAlloc(GetProcessHeap(), 0, hash_size * 2 + 1);

    for(i = 0; i < hash_size; i++)
    {
        buf[i * 2] = hex[hash_buf[i] >> 4];
        buf[i * 2 + 1] = hex[hash_buf[i] & 0xf];
    }
    buf[i * 2] = '\0';

    return buf;
}

static const char *sha1_graphics_a8r8g8b8[] =
{
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "2d9183f4c588ce7ebdce837dd34b6b9f4331378a",
    "0cd0d61e8be5c929f3eff128cff165f40ac8547e",
    "984547a084edcf57c12e3fa3d4afe0e4bff4a1f0",
    "2e2d96ff6d91e5872fa7e28818a8ebbbbe3b1b0c",
    "fec2fe8417e20ca0298ba1de62663ad67197432f",
    "add45a809b7222e2aef33e7ccc1635218b570c0d",
    "ed4ed3c7a434210c48abfa3a8e65eeadf81ae15e",
    "e101e3a5bbdcb829422ef6e90ad0a7f19e9fec27",
    "6e58c93280761828c6b39eec336a137b853f4bee",
    "0bf80f2c9c22f1e719a0e70bd04d2d3dd2d4025c",
    "2877e81b495e8bc9cddd6d516b4bb3ee179c886c",
    "14efc4b458cf2684f95a1a3d01bb5810f372aff7",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "2258c3b3500a3a748ed150c65ce92b75acc83b20",
    "225a666a44c11d29d0ae2fcc79cece32a9236d12",
    "a298c1a8360ed96518f6589d70971495ae1dfb82",
    "a8b305f8df0cefddd54b8fe3b59f1422acf0e43f",
    "dc8eb6ddc6383013a8a57a6967b538cd2722e804",
    "76775c1d3ace3f176d6ab82b6669fa3d31ed386c",
    "984547a084edcf57c12e3fa3d4afe0e4bff4a1f0",
    "d906784715e9586214e48f08f1dfd80a6667225f",
    "3dd7d6046b1e23377667baf0c4484020dabe76f9",
    "b0a51e370ac534afc1f888147df067fbf409722e",
    "c3d3aad57c0ce5c75fe6f0ca4c756f1557d67614",
    "e101e3a5bbdcb829422ef6e90ad0a7f19e9fec27",
    "52548b813888ba471f56ce2e38aa9ffa98f27ce5",
    "53aadb2556ac50509c81b7f6dd89e3074f8907a3",
    "1aa642f53138a9a9d5c8c6356194886b0e405888",
    "55e0c4f17b0d20f5132d7af043b81983c1165df1",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "4404b1502eac7f7d4ca55be14bfdcedf9d7d63b1",
    "23a2f20eaa83782a71c3bfdcb3190c9f9ca09b92",
    "cc6c4e69526ddee6d8f9f4f32498d9a135224835",
    "c77f23c0730d88ad23539f621c90acc4265ffd09",
    "dc8eb6ddc6383013a8a57a6967b538cd2722e804",
    "8b11308894449b2cdb1ae15f326e9226c62ee6f5",
    "5efbf5b9ee4c29edf9b0acd3f283a900288b528a",
    "a94cbf46aee24874adfbe7d74ade52fb515ad422",
    "b919f9f0d76ad82b4535227deb48d8b503b10b1b",
    "bb6aaadeb6536894b6a8348b3a75cdf393fa53e3",
    "5182dc463137012ed6ff20d18fae6cc069098aa2",
    "63add6e32db1d2a5e72d81e76ad8c329282cd362",
    "47197b91b67331d44f9cceb9e73df49d46150b2d",
    "950846d32a82d9552f9dd4e27ec2c4a9bbbf458c",
    "a2196173234a31cb30097f63821360addcb25e54",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "6b3ccd70099df9d24fb26909188218322ba8a81c",
    "87b40f458323da4f734e599944ce9f64327cdd85",
    "0a15a91afea20398be95aaa38c56ecfbee148a4e",
    "287fcde7d39452ca2fb12a893af6c9f8db3a2ceb",
    "37b15d5179f8ce1fce09593db4c0bbde61e915e6",
    "32a66204543744d0eb09456bd987195d809d07e5",
    "aa1e21bab1b91bf495be201ff70a39481aa02f28",
    "60c0a7a2118c7066f41ef44f7ce3cf946a1e05bc",
    "aac8f54a90b6627dfa0b2b84ce1ad434376e3837",
    "aac8f54a90b6627dfa0b2b84ce1ad434376e3837",
    "aac8f54a90b6627dfa0b2b84ce1ad434376e3837",
    "9ac23882cc02cc52603cd321a7eb4c2862d78ecb",
    "01bb8ca627e74c5008f191889688e1d0d3830e6b",
    "42fef6b56cfa1ddeeae07566294696a8f5310f83",
    "22df7b6fa2df43559f288ee72436b296d9143f40",
    "25032eb8f1b03beca7823f4f926b61b3841e538b",
    "98f2e21fedff6ac7e9de282a8e9d13a095b948c3",
    "a859ea82a495de389de3a6281bffbd2f082e166f",
    "56fd8510b3f88103e90895d93f4c06da1ae7afc7",
    "0cbc00ac6d3c2ffc3b980f680b60aa5add53254a",
    "74ffa180af482f95c812117e4002b36425a2350b",
    "5eba4e56ec6f2034b24e86137f6169249f02f10a",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "ad6ba8a3520deaa1b85eb6a1f2b3c767b918b3f8",
    "0a61d3e8d8a3342d45138b1b3779d80351d20e19",
    "c44fd1f38cd19478b5ca89834fa92c192dbc4956",
    "1d520746db6e2578044dadbcd0af265ad93e3c13",
    "9b5c918db43edb0b5eb1e317d1c1a3be2b18eb77",
    NULL
};

static const char *sha1_graphics_r8g8b8[] =
{
    "89a55b8437e04e45a0f4e4006032693e3cf7e018",
    "a608985d40c1515f364d5b7d63769c84d72b377c",
    "8570ebe8521b90dbdf008c3a7e56d2821728da64",
    "a1fdfa4f9bf0ee98a12888a403b8340bc0456549",
    "bd82e8d32e16bf7b81e8f36d9c9de142d15ab917",
    "5e3515c56a5531accfeab0be6a8c9c743614eebc",
    "4be17ae19582246b2d45e0ed8827d01334eb3d82",
    "97e1cc12b0395db79e61e8ac474a82423e4dd2be",
    "daa05e9b3f6f0a57aa9e52088220f794836807f1",
    "c438c8d9d09fbfab54608eab2d456afd2d62b2a6",
    "d79b7672abbbdb115d8e13083410fcae79c71969",
    "2d16e940f822cc3eff7d1f0213f90c9b79d6f82f",
    "7a4302fffb9023ac548add1da89922b92c8eaf92",
    "89a55b8437e04e45a0f4e4006032693e3cf7e018",
    "1340b5b111bffac9af2fcc6bf3b808ad7a61153e",
    "f8227278afaa26e02a0296f6e6e184d07c2d5548",
    "fbcb08a3435d36c78d5d6e233b15b4de094b8719",
    "25c5392d274bc428b8f0202ad1700ae514a68a9e",
    "868bcecb95b24fcaa22aeb06b68199bc1147bad7",
    "8394f86a171172ea21583d0fc14a988c59bdf900",
    "a1fdfa4f9bf0ee98a12888a403b8340bc0456549",
    "849762ba57a64314ed9262346301b53cb7630130",
    "5b9fe77da15b7ea97e5718f520b56c23bb08c6f5",
    "cf4df789ebf667cee6ebc8fbabc28d21896cb68b",
    "5243e3dc13bfb3ae77c09eb439d7ca317814b74b",
    "daa05e9b3f6f0a57aa9e52088220f794836807f1",
    "6d080fbddd62d4a3510d7b6d68e4c1fe2674f739",
    "25666c631404dd699af97111b6014950ae2c64a0",
    "2d24f4d0f4457c2b88fc577e2327826ffad00ae0",
    "9e758fe129ed82b5847a3c897299117411f7f696",
    "89a55b8437e04e45a0f4e4006032693e3cf7e018",
    "ecdd64a9fce08248d47e0c717002a1e8eef8372b",
    "10f29a84a08fdcfc80edc784c7809bf9c7bdc2be",
    "b4dc632d9226d784f29196b59381f034c6d7819c",
    "873afb51875bb28902a69c946d1868628393efdb",
    "868bcecb95b24fcaa22aeb06b68199bc1147bad7",
    "962e428a602a2066c1198ef0d0c7407b669eb4a6",
    "c5679e2988d4e00dc88dde117328f84d474050b6",
    "e56af9c5c80794d4deada57105e2302a0ef2312e",
    "fbd2ff4c301625991605e8b25902ead7313a4da1",
    "8b02a541d56d639931d28fd278181387035d8931",
    "dd1ef92cea2ef2b84fcb1f02485a767809462a96",
    "b9c10d853b9558239de105fac291586194053405",
    "2db1b9a32343dd3bcd769eec44c474b620091331",
    "b289375dd90c2278164626d89c905350fc3d29f2",
    "ad1049360c7244850e618dd9842407fa1d43d79d",
    "89a55b8437e04e45a0f4e4006032693e3cf7e018",
    "fe84004c6f6fa11b46559a85d1955d42d70446af",
    "216ee43535fcc7f25e3f628b806a0bd95aeddfba",
    "a7972f5e442b3e20003061c076b1cd3eaa20e55d",
    "f8cdec74678e6c1a854ad8337234ac29f7679c8b",
    "728da34287ea604991a32dcfa8ef616e03aae7f4",
    "30144dc5959d6ddd744c1a9800fe085efe945c6e",
    "c869ec5ef03fe7ab457adccd35740401c3839635",
    "6ac4273a0fb8518a8e313a808e510e2e190ec60a",
    "71d72b1f16cf670fc8fc45087e924bf5f4e8e485",
    "71d72b1f16cf670fc8fc45087e924bf5f4e8e485",
    "71d72b1f16cf670fc8fc45087e924bf5f4e8e485",
    "42349a2b87efad4a64e7b817c91aa7be3b7a6e5c",
    "f2543dc6186f017412dea8132ae0eb0067b7b34d",
    "cb480395d2e7ef2c6320e7a13c157fc3f3a6ca0a",
    "2f15549c58db97bc7d8fb958b63bac9bf981e380",
    "20ed65c22f86f9c0b99f92f034154ca6d360fc0f",
    "e03f4ac80f3631c356bebf952885c1e02d10f3c7",
    "5d01e5dc5230acdfec8f47412e2ea054ef9793a3",
    "4645c13921523b04d7f2aed5880bfa835df0fec7",
    "4215b2584c30cb65537bd444dd9eff1fc803d14a",
    "6336f354fb31ff4bee34c748a8e2e38064dac0a8",
    "c0b3f5b4c12f5ad24e08e52b3f00fb9ea45853ac",
    "89a55b8437e04e45a0f4e4006032693e3cf7e018",
    "57b9982ceefc840be5ba76c6da547021bf8ea134",
    "00c7cd9892504ce86fa2fae30c6f5c9b534cb2e9",
    "b49a0257cee389ae64eb17c66d7fad8616c92119",
    "80c058f55f6aecf775ec06daaa25c5b4be2ed1ea",
    "019277445c316626ca024af2d39a51ebad7cced3",
    NULL
};

static const char *sha1_graphics_r5g5b5[] =
{
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "78d1ffc49f3e8a22a47a6127fa85b6709eb43d93",
    "a9b85400e42dbd520f7eaf5a9dac7fddafbf2a98",
    "f00e76cb9fcbb93b7e5e0157c3edca8bbf993be1",
    "3bd9c9b32be36276882e8272f05cbf8a59ac500b",
    "49f1be21ccba14e7353dfb9b497ce0a4502a0a99",
    "5b22dcb4a56cd7705ed208fefc4b8257b64032de",
    "79e9bbadbe2bfd8286efe32cfb6dc15ed954774b",
    "f6f8f7bf472f21094b11590315d0c75209b651d8",
    "16cd82b3b79fd31a88c42b77f586aea1370b3279",
    "9333e99c5392fbb8d9a9097d59c097ae416633fb",
    "704850dd6cc451177cd687197a073355b722a75e",
    "9fec3a301e3a76ac5a87b7043d590bcfcf2e990f",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "8492821c8385727a1fbd0f59046f793e4a96073e",
    "f444946ea1b0f3486f139a073f9dec1a14f27d8e",
    "6a0499f50a6443166ecd94a22a24ef7f0b85bbb2",
    "ccb37978304f4ff575ced0267111a23854f94cdb",
    "451edcd867c79a2893c8fc7886aecf67a97baae7",
    "a995533b79920d9e2c5e429d0097bd30f6039ba4",
    "f00e76cb9fcbb93b7e5e0157c3edca8bbf993be1",
    "869c7270650f9b11eb88f9c46a1c01e415005978",
    "019071f4166c48f6a39b9bd720f3fc77b1d5335b",
    "cfcc53121ffd180ed5fa8e462d8663ca3c32a965",
    "762ebe00b05ec7618aa521eb562f25adbbb2bccb",
    "f6f8f7bf472f21094b11590315d0c75209b651d8",
    "88a311356396ad00bf7ced440d5912e06269a0fd",
    "565928c602bc1af0cc48e1ed5730b27d4089bd4d",
    "732ae5801671199e7cd223a11e88dc5768ff4438",
    "ee98bb9670fe054aeaf7ebd99eb2111201ff2c57",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "9e3a577f7d3552e16c5c58e94b00d76edeac2ed5",
    "c26495c9b67a5f2223412e37624e5f6bd60b45d3",
    "783b697cebe9144a853d0ed4d6647c70cf2c39ab",
    "44b9a6acf578e5085eba068e881064d0b6845fdc",
    "451edcd867c79a2893c8fc7886aecf67a97baae7",
    "c63374dcc6c197ebd0ff3b1df19b9789e51cf08f",
    "05452922b7da4581b58beee8e4657aa59e1bd211",
    "26b87abb386f05b5ff4db9e583fdfee17809b069",
    "9a8f08b282227ced4c0bdaa304705c1c5861770f",
    "b719803f1b4870e895d9592205c9942ad2bfd88b",
    "091305e2a9c9acd09ee191fd227f4aae0da5cfd2",
    "1fa3439387dc7e32edcb1bf39d54f852928d30ff",
    "3f5d05e2f7fd9ae3565894c7e6fdd8bb84099b1e",
    "92c50b91f0e517cc678292a1dae951f67fae21c8",
    "5061d2c56dc3dd60b45c173f966347c3dbe2eb9d",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "e647208a067eb3384417034bc348f67e7654eb5c",
    "e9764e69fdddd031c99de0624184337e84be0830",
    "64522bd8aeb34260e58ca1117b7f6c9080b1371b",
    "c6d54e11f9e2d007cf43dd17e742e5eb063e4c99",
    "7311f172af4af582185affbed8d298a477ea3bf3",
    "2e451d57f6ff9fa9bf36580acb248911a8ec8609",
    "a1bac29e4ef66bf67ad86e997e02e4dbbf8e5bb3",
    "b625788a64141334aa2d6580bc5dbc040a81f59e",
    "7af08f5e90dd60d77b352413c9ab80c2e9874cd1",
    "7af08f5e90dd60d77b352413c9ab80c2e9874cd1",
    "7af08f5e90dd60d77b352413c9ab80c2e9874cd1",
    "984cc53040975539828276774ffab20c51a6f72e",
    "a0353f412f2c59b21cba3288568fb75f0ade33f4",
    "78850e855208bd308103a6cfa4d902d9696ef44c",
    "08b429b75a72af746c8237b7eedd21bca7d39b13",
    "1b72f26a052edf8adbecb8da1201f93ca7b52fc5",
    "bca0a377789288b3cab4270d88bd677c33dc4d8e",
    "d491797dd1ad343da5c71dc1471facd575eeed81",
    "cc5fda3dd064edfb563c91057ca1f2b625e9668e",
    "6a398967e54ad4effef852a03674955c0d8ac359",
    "14aa68ae9d3729bb4c1acb7e0216cdeab34a6979",
    "1a32c080eb0ce7122aa5b56859547e270d64d23f",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "6a052feaa6989729f8ad01c721814c7e553021c5",
    "b48aae72b6b85bedb0187260aeafd5759c689206",
    "5bd842474d9c4c9b9a68fd045f3a5debf51c6d17",
    "e90804ac77bdb4034fbb5a948a1963133d67170f",
    "e53c7e15df7f8a6371f5702b4cd67a7083d54c1f",
    NULL
};

static const char *sha1_graphics_a8b8g8r8[] =
{
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "e383c1071e40e6b430dce07362b4f8f08d1a2664",
    "baa7320a6d3a022a437fc83de4b87043550431bc",
    "984547a084edcf57c12e3fa3d4afe0e4bff4a1f0",
    "bae34003c7c98f48ca3745d42bc4e8e16ca6dc83",
    "9191f0c7efe75bdc56583e4084da831608426915",
    "e027dcc94aae26b56474c68b14e29bbde881e460",
    "71cdf94baffc177fb021627b71f6e8289e2e9243",
    "e101e3a5bbdcb829422ef6e90ad0a7f19e9fec27",
    "ea64721a62d38c6a293120eb9dcfbfe3e0b8a68d",
    "87a536ea8c7c3274fcab204ef6f729c4cfc756ef",
    "2ddffa82b883fa337fa7ba4f407c367f7a4620cb",
    "eb8234894c1d94a2b547ab1c853e9ed23fadfc80",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "fb079298dd9442a7ba2b4d1c6da2261e8f9193f1",
    "6ddd366e3b64c6cc89936fe4cf182e18953bd162",
    "4770abc73f51a0ff7dbc8a2a4c1c7c59a0a06df4",
    "b69724f3335c51eb932bdc920434771e04d14855",
    "dc8eb6ddc6383013a8a57a6967b538cd2722e804",
    "8602d2083d40cc735ea7b3b0087d06072ae9bd73",
    "984547a084edcf57c12e3fa3d4afe0e4bff4a1f0",
    "41655e6a94abc4fbb549c4d057bd30f74b5a84f9",
    "0e861936a0db81d849759b972ab2647add9796c4",
    "8e2116d5d7cd28f11cb7998195a3b28d1aa992c0",
    "0123043adc915402cad13410411f90d4674cb306",
    "e101e3a5bbdcb829422ef6e90ad0a7f19e9fec27",
    "505bf0effbf8faae4bb6141974b06717fc69d219",
    "bedbaf31e251bbed87e613aad146307dc1b6c6b3",
    "4e23960b94606c8adeb81f75a6526504761b1734",
    "f3dafa188ce192faca4d94786c43b2c9f7431702",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "7765956fccb1c3c24436bc513b40c67483c3e104",
    "fa5f91ed2e7667b2d681109837475f10be116139",
    "610f232a3171ea8ee9f2a7493d202f4d597a4422",
    "610c13cf34f5d944f3bb13cfc264b2bed24e4a3a",
    "dc8eb6ddc6383013a8a57a6967b538cd2722e804",
    "8b11308894449b2cdb1ae15f326e9226c62ee6f5",
    "5efbf5b9ee4c29edf9b0acd3f283a900288b528a",
    "a94cbf46aee24874adfbe7d74ade52fb515ad422",
    "b919f9f0d76ad82b4535227deb48d8b503b10b1b",
    "bb6aaadeb6536894b6a8348b3a75cdf393fa53e3",
    "5182dc463137012ed6ff20d18fae6cc069098aa2",
    "63add6e32db1d2a5e72d81e76ad8c329282cd362",
    "47197b91b67331d44f9cceb9e73df49d46150b2d",
    "950846d32a82d9552f9dd4e27ec2c4a9bbbf458c",
    "a2196173234a31cb30097f63821360addcb25e54",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "6b3ccd70099df9d24fb26909188218322ba8a81c",
    "87b40f458323da4f734e599944ce9f64327cdd85",
    "0a15a91afea20398be95aaa38c56ecfbee148a4e",
    "287fcde7d39452ca2fb12a893af6c9f8db3a2ceb",
    "37b15d5179f8ce1fce09593db4c0bbde61e915e6",
    "6fae7712434f428c58ff3b3d1b530411c7288ff2",
    "3246fc235d8d7738454c1cf1d75a9a5e342605a3",
    "d2d5be685c90c5bdca73bc1dd9cba18fe01dd90c",
    "b9ca3bb24d47276f2d9f3ebde2f8212ec3012a7c",
    "b9ca3bb24d47276f2d9f3ebde2f8212ec3012a7c",
    "b9ca3bb24d47276f2d9f3ebde2f8212ec3012a7c",
    "1487d5cb1d2044d9d5ab5490117139cfbd733db5",
    "01bb8ca627e74c5008f191889688e1d0d3830e6b",
    "4eceae78b0a3c0d7d42568cd6f3826f14d1b0cde",
    "f8066f6decd926a09c5ce993550280d1283526dd",
    "194f101d1978f5c770a63472e00d1e9fcec7b9f8",
    "51782b783934bdb93332d1cdd9a6828b888a7925",
    "a859ea82a495de389de3a6281bffbd2f082e166f",
    "2fe5923a3d73ae9be37be6aec2290ff1c4a84998",
    "6b682bba1584ef44acc6ff64c9a2c2150d156286",
    "e1db654f1c368204a990ec5f40c4f999371f3901",
    "ceb3948d9d2bdc541a813b388de9390980b3e125",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "599cd0943f68d91525b18bf023c6146a2abcbc10",
    "e29d67d6fb6b9824c312eb43e5d80263ebfe99c7",
    "80fcb1ab5cfd10e34343ec7d68b87ff7146e7239",
    "46246a256de6eb2d69e6d615177352e23569b0d8",
    "9b5c918db43edb0b5eb1e317d1c1a3be2b18eb77",
    NULL
};

static const char *sha1_graphics_a5r9g9b9[] =
{
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "7068a270d021bae6b818ee7fee380a195fa9a313",
    "e7f686e271af7cbf4b102a85eb6a5f63cd8878ce",
    "984547a084edcf57c12e3fa3d4afe0e4bff4a1f0",
    "dfc4eb6831c242b264517c4e1c2c4650e200239f",
    "fec2fe8417e20ca0298ba1de62663ad67197432f",
    "2dd24904813503f7fbc36e1b9a5d198ca78c47a8",
    "0026235d4a23454471cdb4f6ef95716221541932",
    "e101e3a5bbdcb829422ef6e90ad0a7f19e9fec27",
    "49ced2410172c99978f4fda1c4f177447fcc700e",
    "0bf80f2c9c22f1e719a0e70bd04d2d3dd2d4025c",
    "2877e81b495e8bc9cddd6d516b4bb3ee179c886c",
    "734dc770f6f09563fd0284f9be16cdd587f5d503",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "e77dd688f82143b87e37cc42cb428a75ba00dbd5",
    "30359c04c4075a9207ead667181ea9123f6f6439",
    "a298c1a8360ed96518f6589d70971495ae1dfb82",
    "8e782acd734e091f2bdf303a10c4302c133c040c",
    "dc8eb6ddc6383013a8a57a6967b538cd2722e804",
    "f6d22e3d490113221317c6dbbca2fc2cc9bce663",
    "984547a084edcf57c12e3fa3d4afe0e4bff4a1f0",
    "3a5b7eb2c5eefa90a6e5ebb99667fd65db3aab80",
    "66ab6a6d8af2967897b96b4bb870fff5489c01ac",
    "913e4f0f441c72f0ef98a6786ea0cbd54b73b917",
    "950ad5df4037bf8d9147c36521de3a12b2577263",
    "e101e3a5bbdcb829422ef6e90ad0a7f19e9fec27",
    "3992bce5515d6054ae7973dace9838dd7dacdeaf",
    "51b2ba51766b31ae2a2cca5eca6387e68f4fe9e0",
    "8c2023828a64d2bf0606318613a8152cb4f46687",
    "bb732feb2b26ecf8eb58155445b51128df071328",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "9a1199ece40e6cdc9d3f8f89608ae950ca98bfaf",
    "5c5dd8acd958a516488fead1c21f9bc2974343c9",
    "abd77166ce6340fa2e206e635ad43ed8aed599b7",
    "2e314044b5f785e02c6f91e5dfeaf52c7786743b",
    "dc8eb6ddc6383013a8a57a6967b538cd2722e804",
    "8b11308894449b2cdb1ae15f326e9226c62ee6f5",
    "0a2c8620d97b0f9efded382bd19b50c2b329c687",
    "a94cbf46aee24874adfbe7d74ade52fb515ad422",
    "029b327de981455ab87bbc54f85446f8de6e7cac",
    "2e4694ee4ffa43fb59ba3d9e3fe11dac1dad91e5",
    "5182dc463137012ed6ff20d18fae6cc069098aa2",
    "44becddea4b7c61d112c3756e2d435432b869108",
    "47197b91b67331d44f9cceb9e73df49d46150b2d",
    "950846d32a82d9552f9dd4e27ec2c4a9bbbf458c",
    "a59b05dc53a0a605af13d88c2d108ec0d3157b38",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "2d2b5a876792e3cea374e61bb8b58a8c43575da5",
    "0cb16e487af160c9c4c4aa0c66b40ce3b5c1e989",
    "0a15a91afea20398be95aaa38c56ecfbee148a4e",
    "dc79a0197f17edc72d1fd28fc230001f19300212",
    "37b15d5179f8ce1fce09593db4c0bbde61e915e6",
    "126fcaae356fa81c68a33b4513072b32e7e79463",
    "de203f0b653aa274311a4fbb9c8733916c569128",
    "e9491902bf39eec6e72bd6d44a4d4bdeb420e5d2",
    "9382169de47c9c95b210cb617d98df28ad12b610",
    "9382169de47c9c95b210cb617d98df28ad12b610",
    "9382169de47c9c95b210cb617d98df28ad12b610",
    "c6ca46b4ee4b64747598bf08f8420982df402afb",
    "01bb8ca627e74c5008f191889688e1d0d3830e6b",
    "807399db85c6ef265d758dc928e844e7e1656bcd",
    "b76e40e7af557d0116875c1f62b53055905487a6",
    "f5bb814b50509e23d38978abfb3afd75e02c20d7",
    "4ab2593f8bbe6b7da4b75eabbc1be5ff9756b02e",
    "a859ea82a495de389de3a6281bffbd2f082e166f",
    "b0b0deb60020f595af949c39722f729881726bda",
    "3af587598acf0f0b9aab3341915cc38b8efdce17",
    "a8ba923387eda65d91a4478e669b93dd27e6977d",
    "7a84d687c8afecc89099d7c51db1fb16825287e6",
    "95cb7f0f4368270ae675522c797e6f6ab588ef7b",
    "adbf382c2e87765c981287fa678c6d764b87cd25",
    "d175d888ba38358c68e30ee44b9b637479a4203a",
    "e14213ec869179f40686934110514cdea45a6ae7",
    "d8056699ccd789dec0ad7ac331d78aac4f32cdb6",
    "9b5c918db43edb0b5eb1e317d1c1a3be2b18eb77",
    NULL
};

static const char *sha1_graphics_r5g6b5[] =
{
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "78d1ffc49f3e8a22a47a6127fa85b6709eb43d93",
    "a9b85400e42dbd520f7eaf5a9dac7fddafbf2a98",
    "f00e76cb9fcbb93b7e5e0157c3edca8bbf993be1",
    "3bd9c9b32be36276882e8272f05cbf8a59ac500b",
    "49f1be21ccba14e7353dfb9b497ce0a4502a0a99",
    "5b22dcb4a56cd7705ed208fefc4b8257b64032de",
    "79e9bbadbe2bfd8286efe32cfb6dc15ed954774b",
    "f6f8f7bf472f21094b11590315d0c75209b651d8",
    "16cd82b3b79fd31a88c42b77f586aea1370b3279",
    "9333e99c5392fbb8d9a9097d59c097ae416633fb",
    "704850dd6cc451177cd687197a073355b722a75e",
    "9fec3a301e3a76ac5a87b7043d590bcfcf2e990f",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "8492821c8385727a1fbd0f59046f793e4a96073e",
    "f444946ea1b0f3486f139a073f9dec1a14f27d8e",
    "6a0499f50a6443166ecd94a22a24ef7f0b85bbb2",
    "ccb37978304f4ff575ced0267111a23854f94cdb",
    "451edcd867c79a2893c8fc7886aecf67a97baae7",
    "3381be35fbe1d3d27442beefea9f3559167d63a0",
    "f00e76cb9fcbb93b7e5e0157c3edca8bbf993be1",
    "d25c0c7199b7a577f6a0e4355072d29c98f0f9b7",
    "2c0d002266f1a559b00d785e28191c5b36a38c2f",
    "296815195c84cdca49e4cc69b796727c9679817b",
    "bded9b2c2ae58da0974ef8d424bdfdc6e9d57c4d",
    "f6f8f7bf472f21094b11590315d0c75209b651d8",
    "7ba0bb3eec5153e7eb98a016cd60d934a0d8cb48",
    "c4f1f181ebebd2e38a2f52da8de509fa4487f8ce",
    "20c5dfb32fbacdfce928bab65e40657bd77b5698",
    "55a7fd397525225bf8881c9b0dd1070a5e8e6d31",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "8c29ce4aa19f4d461a4b3a3862212316624c8ce4",
    "2b386555f98eae605e2216297800dd3119633ed6",
    "9a77e4323b0bd99f247f7211478ea1b8226ade81",
    "4ca3a38fe0e01bfc539373eab6003c300677480d",
    "451edcd867c79a2893c8fc7886aecf67a97baae7",
    "c63374dcc6c197ebd0ff3b1df19b9789e51cf08f",
    "5d7599a63c6555912d0b53bb5a9c0c6a55beecaf",
    "8293b0afa75382b1b88b738737e3288cb75a870f",
    "7bd064a1e9f57b22a5504171c1e82ccf508ab07b",
    "cb59da5015362ad2238c3c358a5649b2863d427b",
    "091305e2a9c9acd09ee191fd227f4aae0da5cfd2",
    "c63d0af64439d57fe15d67fd0d2874a3108d1538",
    "46e519fedb8b4b3169a072714a24b083aa702d48",
    "7ecf06acb670373d4d878316008fa9fd970a0750",
    "04676f8f0899a802c91393f9ad3409f4f3140e06",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "e85e1cdd46c60fe6d67d2eab94060730c95e7e7a",
    "6937b5273edbcc3aa46411c37ad43be69d5df307",
    "e45df7e337d3e1865a0233fa744d5108e361bbd9",
    "a53b9b2497ed07ab725942d731e223378e007d34",
    "7311f172af4af582185affbed8d298a477ea3bf3",
    "77fc492c8b6f876ad0cfa90f6dbff18c0fad1190",
    "deb15444e7e9334424531f18832b28d823c58876",
    "92a247d59c595f52a294800574db83b363562465",
    "434adaa9d11e593c339600ff729a065a5c99deed",
    "434adaa9d11e593c339600ff729a065a5c99deed",
    "434adaa9d11e593c339600ff729a065a5c99deed",
    "0abc55c21410893dc1b10dadbea66637762f3e97",
    "a0353f412f2c59b21cba3288568fb75f0ade33f4",
    "dece28d57e60e4495ba3f317d4cfb14c697d2117",
    "ddce1ba3814b976ac1401450015100152b105380",
    "e60d78768ea00e9ff742716e9c0f0decc8c47935",
    "be41b9da9bc3eb43c2fe1089db00a55915b360b3",
    "d491797dd1ad343da5c71dc1471facd575eeed81",
    "723b29d4ad771ec86bf204738be9904361bcb5c9",
    "214c0791b1d0aa023822c68352243b1cfb3406c8",
    "50be8792d83a6f819110910d41843d9121fe0926",
    "87e481aeed2ef38e8358e2669547e145ecf60ed7",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "54f6375d30dcaed8505db4578cdbc08fb4471a87",
    "b36f7882e06cc6f994760aa9dcb14dd60abdca9d",
    "0c86b204c3db2c51d8be1571045e6304e1d65559",
    "9f0bf101a38597cb7af09ff826f60264b553621f",
    "e53c7e15df7f8a6371f5702b4cd67a7083d54c1f",
    NULL
};

static const char *sha1_graphics_r4g5b4[] =
{
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "919ba89672b906b052df5c225ddbaf8e6474390f",
    "d0cdad9dd3668bb3c3c48699cb97623a99e62ae1",
    "f00e76cb9fcbb93b7e5e0157c3edca8bbf993be1",
    "1c0a5b7e02e464e77b173909bf52b0475bf23a59",
    "49f1be21ccba14e7353dfb9b497ce0a4502a0a99",
    "30a3133fbb6f5cc60aacd348148aa44b0121bd0b",
    "ec68b7751e2d19e01aa2515e0f5539d971b81255",
    "f6f8f7bf472f21094b11590315d0c75209b651d8",
    "87156a3f8f142258b52806f42735f4c7cf21b7f1",
    "9333e99c5392fbb8d9a9097d59c097ae416633fb",
    "704850dd6cc451177cd687197a073355b722a75e",
    "ff1972911441802da0a361abec3b113ef60613ef",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "5993b550845cc2510cefa6325dfd29a7569847e8",
    "f145873125f7e1ef0b80cf6c1a898c0f2b71f4a0",
    "6a0499f50a6443166ecd94a22a24ef7f0b85bbb2",
    "c2509287ce5e765261941346f55c99224bec5d17",
    "451edcd867c79a2893c8fc7886aecf67a97baae7",
    "25bfd9ede376e33e88e7d594f2d88a5c884500c5",
    "f00e76cb9fcbb93b7e5e0157c3edca8bbf993be1",
    "9b510d19f6f97b9387de3b18e2ec860a0f21e979",
    "7304471235626e736efd85d4d3648fd130c1dec1",
    "8ddb401072fb18798336abed6208b19be9c870eb",
    "76ab54b969ef1179e5f7992899cb8351c0f98afb",
    "f6f8f7bf472f21094b11590315d0c75209b651d8",
    "876b8065841d05a72f8bc3d6dd10401ae75135b3",
    "f690d657f419a6f3838700468260bbce2ef2a781",
    "841c414450e2b1562f24f9603c7ec27af7cc2e0f",
    "b2adecc530f4901dd70d11363ded31f5628b0766",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "b2864f6bc35549cecf4988fef6415476f2043c1a",
    "8f669ad5291dc2738016b0352678bf2127a07f48",
    "ea48662d442d1273a21d7fb63d40580a2c67f7f2",
    "7dac4137a2cb9e8a96b24c70bee7c5bd3e646457",
    "451edcd867c79a2893c8fc7886aecf67a97baae7",
    "c63374dcc6c197ebd0ff3b1df19b9789e51cf08f",
    "6a02f0297423aa392d0dfa8657f61bdbc66f4dd3",
    "8293b0afa75382b1b88b738737e3288cb75a870f",
    "435c1484306443a9df0d6ba3f10ca4da2537ffef",
    "15cf6652d68f9d612918795505082f36f66c45a6",
    "091305e2a9c9acd09ee191fd227f4aae0da5cfd2",
    "eb5753f32fdff5ff219e98b33ed6db234187560a",
    "46e519fedb8b4b3169a072714a24b083aa702d48",
    "7ecf06acb670373d4d878316008fa9fd970a0750",
    "6f052d8b979bd31b5c6b22a9cdeb175774aff3ea",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "702a3901b2c17a03499794df09763f2eb07dddb9",
    "e10e6e6824f76ba5b6c4e158513178cfff0c14e1",
    "e45df7e337d3e1865a0233fa744d5108e361bbd9",
    "430b98ff04d75db0a2a1ed81974f6dbbfff269b8",
    "7311f172af4af582185affbed8d298a477ea3bf3",
    "c15351ba1f6b0717d6eb74ec891bb776ebfb7bb7",
    "069f98a71539630baf8bea1bcd413fb6fe192408",
    "a4218a71300c1235d746e031063e06e58d110852",
    "b013279ce9bfbd3e6c276b279a4987fb8dc5805b",
    "b013279ce9bfbd3e6c276b279a4987fb8dc5805b",
    "b013279ce9bfbd3e6c276b279a4987fb8dc5805b",
    "d3eee685884720aaa6bbe9112748a7dd823b0f7e",
    "a0353f412f2c59b21cba3288568fb75f0ade33f4",
    "2d090fd014a530e5addbd03ada88e7bb5b0e4e07",
    "81a2e058eb7e8f4058f22995b25b3c0ab2e1bbf8",
    "7a60f4b53cc6588c6d1465b5d1cfdab945fd8fc1",
    "19f57c51385003e9bbd912074c2577edc0c345e3",
    "d491797dd1ad343da5c71dc1471facd575eeed81",
    "dde3f079389a2c1f5a65764a2962f7bad6351621",
    "96c775c7acf13339c7bdb3e967e3fced68bedc22",
    "746dee730270e6c5e2cb508323dbd0dacfe3b7ec",
    "a059e885935b78271ab3f2a590668cbcec2757f7",
    "7e3f989ce484ca33da1285d16c249a1cca615f8a",
    "0d31dd01ccc678adc1f9f7150471c0048e982408",
    "974e30c2f3e719edcecc026af07743be055bf5b5",
    "c2fbffc7f1e3198262ecd7f8a833ff1c5505427c",
    "7b4887ee234460f3beda1936f2258492f25f3318",
    "e53c7e15df7f8a6371f5702b4cd67a7083d54c1f",
    NULL
};

static const char *sha1_graphics_8[] =
{
    "76c12085cc663150d792db4a63d32f4bcf005695",
    "49f8e672f1d2c77007196ec96f184e7a40ee6fdc",
    "ee9cbd1a485b469bfb04782bfe7182fcf6d2d25e",
    "bc31e57d62e5fffc45db48f7c0bca525ad54c544",
    "6d6630d434beacc32dc785520a98d120dc5314e9",
    "a2572ac555b4615e22d08f6bb921924ad99ebbb8",
    "dd41e413044a8fd1b8a4d3c0d34817ded0109e57",
    "bc31e57d62e5fffc45db48f7c0bca525ad54c544",
    "6d6630d434beacc32dc785520a98d120dc5314e9",
    "a2572ac555b4615e22d08f6bb921924ad99ebbb8",
    "dd41e413044a8fd1b8a4d3c0d34817ded0109e57",
    "6267cc2712f89b1940098c345b81e20d630af242",
    "7850b8bc680072607f6aa483b00f3b51c61a084b",
    "76c12085cc663150d792db4a63d32f4bcf005695",
    "bf5ca681587c1f85e540c69f592aa38a0fdacd3d",
    "6267cc2712f89b1940098c345b81e20d630af242",
    "7850b8bc680072607f6aa483b00f3b51c61a084b",
    "76c12085cc663150d792db4a63d32f4bcf005695",
    "bf5ca681587c1f85e540c69f592aa38a0fdacd3d",
    "bd8535fa17b278166e53a4443914f3984a599a73",
    "bc31e57d62e5fffc45db48f7c0bca525ad54c544",
    "abe9656d744d40a26d7da30fae4c88be8af74c44",
    "f9ace8b101e1344451247c1964c238b2e0bc0bb1",
    "c8c77890322204374cc1a7d157212038705d3e4c",
    "681477ed2bb74cbf49dc75458730c2c5fe90cf35",
    "6d6630d434beacc32dc785520a98d120dc5314e9",
    "e03e01ff4748e0b1dc3247400cf865abead69dbd",
    "7281d4499acc4bb94a7d1017de2086e517553523",
    "a062434f58b881b605b763e79fe8db86a5ee12a1",
    "656ee34d1064316910927888e3f30c5a063f47b0",
    "76c12085cc663150d792db4a63d32f4bcf005695",
    "92d040ec8f9a00294bbb5292c817174e68c609f7",
    "04b3bbd08130efd0f8d165cc337208fd9f7461b7",
    "262d40693ec42d5d9e27f10c96006c344e279017",
    "12fad6eaca662a509dca03cd31fba1ce3ff5d1a5",
    "bf5ca681587c1f85e540c69f592aa38a0fdacd3d",
    "1dc5d92ddf20691b64858717ae2c267a66c95ff8",
    "c981529d1f5291eab4fd4fa51c78b337cf5beb0f",
    "76c12085cc663150d792db4a63d32f4bcf005695",
    "1e639ad41f1fd2b445777878dd45ed3bc023238a",
    "39d783b361beee78f1dc9fec24f54852397a41c8",
    "e5e656688fe28cafe645589babba8aa3b2409d01",
    "12fad6eaca662a509dca03cd31fba1ce3ff5d1a5",
    "dcffb608d118bf444339a57425f6c00ad40c8def",
    "1dc5d92ddf20691b64858717ae2c267a66c95ff8",
    "c981529d1f5291eab4fd4fa51c78b337cf5beb0f",
    "76c12085cc663150d792db4a63d32f4bcf005695",
    "1e639ad41f1fd2b445777878dd45ed3bc023238a",
    "39d783b361beee78f1dc9fec24f54852397a41c8",
    "e5e656688fe28cafe645589babba8aa3b2409d01",
    "12fad6eaca662a509dca03cd31fba1ce3ff5d1a5",
    "dcffb608d118bf444339a57425f6c00ad40c8def",
    "4e6a371f2d72429ace2741c1ee5a484fbdbc0124",
    "3f2cbc3494bd5f2f7c446ccd42f8eebf15559004",
    "16ee1078269940e5406e56ea0d5392075266901f",
    "aa06c4f3fa2a6a05ca24b9546b9680bbf240e22d",
    "aa06c4f3fa2a6a05ca24b9546b9680bbf240e22d",
    "aa06c4f3fa2a6a05ca24b9546b9680bbf240e22d",
    "002ee83fa21a7c7d10c0bcdd442508054c9a2696",
    "4801fa201b21ed94c8d33161f7e138075dc434a3",
    "8228751e0bad3903b35b3d5cdb90052b448063d7",
    "76c12085cc663150d792db4a63d32f4bcf005695",
    "359adbba637ee67fdf250913ded6b0131cc2e38d",
    "bda3c8bef9b5db6734bc94ad5924aedb8738d13f",
    "2610efe0999428184a6cf736f5c7d83e6e4eab62",
    "21472bb637d79a12af8ee6ba24aa1a321a332822",
    "6fd35a6fa2a7f157e32c408dd3506ab42e755549",
    "4801fa201b21ed94c8d33161f7e138075dc434a3",
    "8228751e0bad3903b35b3d5cdb90052b448063d7",
    "76c12085cc663150d792db4a63d32f4bcf005695",
    "359adbba637ee67fdf250913ded6b0131cc2e38d",
    "bda3c8bef9b5db6734bc94ad5924aedb8738d13f",
    "2610efe0999428184a6cf736f5c7d83e6e4eab62",
    "21472bb637d79a12af8ee6ba24aa1a321a332822",
    "6fd35a6fa2a7f157e32c408dd3506ab42e755549",
    NULL
};

static const char *sha1_graphics_4[] =
{
    "b8f5f858da37fd16f13658bcd8b01e0aa267d93b",
    "0cb0755deb9f203d41e526704508712731d4775f",
    "fdc7f47c61706208aa354a7978b35552aad2c7a2",
    "ac7f98e13a8e0e51edbb789b3a6dbd9ccf8774b7",
    "aeeda45bb572aef5cb3e76a329c8e25470adc763",
    "71423336495d7ca61ef688c695da3c0bf938fbb6",
    "eaebe115722689ab5cdc968613d6ed1dd60cd792",
    "ac7f98e13a8e0e51edbb789b3a6dbd9ccf8774b7",
    "aeeda45bb572aef5cb3e76a329c8e25470adc763",
    "71423336495d7ca61ef688c695da3c0bf938fbb6",
    "eaebe115722689ab5cdc968613d6ed1dd60cd792",
    "facbe32a70a05ab5607e16c5767290b02c92a28c",
    "1f60c6493cf11837eb76fb46ef95fef4ceefc419",
    "b8f5f858da37fd16f13658bcd8b01e0aa267d93b",
    "c58da8612b8f17c3f332d9c03856a25734999b65",
    "facbe32a70a05ab5607e16c5767290b02c92a28c",
    "1f60c6493cf11837eb76fb46ef95fef4ceefc419",
    "b8f5f858da37fd16f13658bcd8b01e0aa267d93b",
    "c58da8612b8f17c3f332d9c03856a25734999b65",
    "39d14d1bd97950e3b0e12b5551a48551b4c9f2e9",
    "ac7f98e13a8e0e51edbb789b3a6dbd9ccf8774b7",
    "41c28ddfb26ce8a7f5036025cf547a01638511a5",
    "a914d1878f575c6655d6c1a47b3eea3ba15c86cb",
    "73bb8928051f9067fe6468033551b2f0842afb3f",
    "4b7152af08bc5d9e3a420c564b712a36725bbfb1",
    "aeeda45bb572aef5cb3e76a329c8e25470adc763",
    "0160d0a3f6c9b22c24774ae79ca5da2057dc87a2",
    "488cf1a87a433d14b9c366aac8847233ca0f6b54",
    "2e146eeb4a2aa06f437c64d31c77b036a66bdb9f",
    "ddc828ce634b33774a1d57c02a0d0918cc2edf21",
    "b8f5f858da37fd16f13658bcd8b01e0aa267d93b",
    "d27d72f66aeeebbcda0845eec8f875e2e6af30ee",
    "01f53e854c982ccfe4448beeca149372d53aacbc",
    "385a5c1c6e67838af629c1b3c9eff653662a9a0a",
    "eda6df01682b30f994d14f14b499a2ce69b6aef7",
    "c58da8612b8f17c3f332d9c03856a25734999b65",
    "f3413c5174c5545a49c695e5574ff280aa4d3c21",
    "ddebb1638c1736a07eef7bf01ea7ac413105444d",
    "b8f5f858da37fd16f13658bcd8b01e0aa267d93b",
    "786d60e6c4abfc864499d1327fdb348c03caccb0",
    "3afa9a623504486fa235a98fd2bcf3617fd47cff",
    "9d5e84bbee6391c0d02c68d0e87b0264af152f7a",
    "eda6df01682b30f994d14f14b499a2ce69b6aef7",
    "1e343d0889d7cc13aa0f66d54b834c163eb6da07",
    "f3413c5174c5545a49c695e5574ff280aa4d3c21",
    "ddebb1638c1736a07eef7bf01ea7ac413105444d",
    "b8f5f858da37fd16f13658bcd8b01e0aa267d93b",
    "786d60e6c4abfc864499d1327fdb348c03caccb0",
    "3afa9a623504486fa235a98fd2bcf3617fd47cff",
    "9d5e84bbee6391c0d02c68d0e87b0264af152f7a",
    "eda6df01682b30f994d14f14b499a2ce69b6aef7",
    "1e343d0889d7cc13aa0f66d54b834c163eb6da07",
    "1c86ed72ae10eb710c8117f06549cc3ab09412e2",
    "abd3590c41a7034f10bc0ea31f07eb8ac906660f",
    "ea3ea378b7ed86f5a6a1989f229d7fc025616f2e",
    "8470d91312c6ab646910db50c673ca5ef1bb3d85",
    "8470d91312c6ab646910db50c673ca5ef1bb3d85",
    "8470d91312c6ab646910db50c673ca5ef1bb3d85",
    "af6eac13b1cf1998eac6e415f1325795d4d9246c",
    "30e5ceca1e9f522f620e8638d1a2e5831c73949e",
    "ef62489cfdb257a984071c533136f699f8c16391",
    "b8f5f858da37fd16f13658bcd8b01e0aa267d93b",
    "19252202802e3ef7a4523a4071dcf7795f1b2fd0",
    "a3ef94d5ff13da7d40dedb649d63e28a63107b1a",
    "d125d2966672b974d03af35d5fba3495048fa128",
    "0a5babb6125077f68b7ae0105f962b56b5343d72",
    "18033640b6708a08093b4964dabc13def090bc52",
    "30e5ceca1e9f522f620e8638d1a2e5831c73949e",
    "ef62489cfdb257a984071c533136f699f8c16391",
    "b8f5f858da37fd16f13658bcd8b01e0aa267d93b",
    "19252202802e3ef7a4523a4071dcf7795f1b2fd0",
    "a3ef94d5ff13da7d40dedb649d63e28a63107b1a",
    "d125d2966672b974d03af35d5fba3495048fa128",
    "0a5babb6125077f68b7ae0105f962b56b5343d72",
    "18033640b6708a08093b4964dabc13def090bc52",
    NULL
};

static const char *sha1_graphics_1[] =
{
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "f3223733d1c595f2ad4de9a0261a918f165f6ed9",
    "2d970c54845df1a154197eba603266f86aec76bb",
    "b2fa2294db6d493d7df9643e505dcbc58178be50",
    "b2fa2294db6d493d7df9643e505dcbc58178be50",
    "b2fa2294db6d493d7df9643e505dcbc58178be50",
    "b2fa2294db6d493d7df9643e505dcbc58178be50",
    "73f5d424431298b3a514334454693730755b2f05",
    "73f5d424431298b3a514334454693730755b2f05",
    "73f5d424431298b3a514334454693730755b2f05",
    "73f5d424431298b3a514334454693730755b2f05",
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "068dc69cc8e2f55606d7fd9e96cadd4dc83fc60e",
    "068dc69cc8e2f55606d7fd9e96cadd4dc83fc60e",
    "068dc69cc8e2f55606d7fd9e96cadd4dc83fc60e",
    "068dc69cc8e2f55606d7fd9e96cadd4dc83fc60e",
    "167b965345ffb2ce1e8363cfec90564f27778277",
    "b2fa2294db6d493d7df9643e505dcbc58178be50",
    "85cab756c540f49661015be28dd9b0c33ed16191",
    "eaff39417a97e35fbc3487d7fb9cfd187f50fc4c",
    "786dd3d6466d7846f77d175dec49b1db7dfcfd9f",
    "9d947bdf38f455420f56e30f734c5c681a9fc35f",
    "73f5d424431298b3a514334454693730755b2f05",
    "5b61b93fcccd03321b42375a50a91ae4a02c876e",
    "df13a0145bda379cf1dd620963dee14dd39afc62",
    "69542a5c06c1f4c780fb0c18d41a52ff2f3579dd",
    "51f0fb11aae203a071bbac8d6cc48b67e294b637",
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "9678635452d8b33651b4a4dc354b910cc8548178",
    "83bf7dc9f9dcb583194198099be368f7610808ae",
    "a89b47f19c6e791c35a36dd91c9d26ec9a513980",
    "0f4a556b17bfee2445fd5d4b3ee13d32d0e9fd97",
    "068dc69cc8e2f55606d7fd9e96cadd4dc83fc60e",
    "69542a5c06c1f4c780fb0c18d41a52ff2f3579dd",
    "51f0fb11aae203a071bbac8d6cc48b67e294b637",
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "9678635452d8b33651b4a4dc354b910cc8548178",
    "69542a5c06c1f4c780fb0c18d41a52ff2f3579dd",
    "51f0fb11aae203a071bbac8d6cc48b67e294b637",
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "9678635452d8b33651b4a4dc354b910cc8548178",
    "69542a5c06c1f4c780fb0c18d41a52ff2f3579dd",
    "51f0fb11aae203a071bbac8d6cc48b67e294b637",
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "9678635452d8b33651b4a4dc354b910cc8548178",
    "69542a5c06c1f4c780fb0c18d41a52ff2f3579dd",
    "51f0fb11aae203a071bbac8d6cc48b67e294b637",
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "9678635452d8b33651b4a4dc354b910cc8548178",
    "efcc457f75daee52ba0936afce607fc1d4e70a14",
    "fd93705aa21b1953f5c0d8fd97f8c3f1fe14acec",
    "bd53007da4502db088420c1d60db67ccf2cc25be",
    "596adccf7333db83f72885464b8f9bdc88a26a0d",
    "596adccf7333db83f72885464b8f9bdc88a26a0d",
    "596adccf7333db83f72885464b8f9bdc88a26a0d",
    "b11a1d182490232e2a77251bddd5909d4a9d85fa",
    "03af60d5d3549a89fc506d0477fdf1e2c0ca7c45",
    "5c58e51fc31acfd51b3ee3babe09d5fd1b0d1df7",
    "cde2fe486b4ab1b89c0591cfd59872a6bace2c32",
    "4b48d864d45bf2c7a5fc937afd2bada8ea49a372",
    "d8fd42abbb9db3f32e2699d747426fde0458b3bd",
    "5f8544b4dd13a2cf6e40f8a5732788479f07193b",
    "a92ac3bade3f2918af1f92c8c4831dacaf07fa0d",
    "42df04e11c0929d4eb1a87d95af826734161ee66",
    "71e27b14baa85a46bbbaaf3a77ab870a69b3f920",
    "6c10e2f75f1a7e3a1c4c6d2163237e8b6a90eded",
    "0eab6335558ea0b370f791595d15d71c6aca6c96",
    "3c2b2ce337c8fd2e9582d4b61afc4a32f4d7d785",
    "1ad4037f45c8741b92603153b659aa3b71fbce80",
    "483f924e026c94658c93840f06cd912b65b2baef",
    "80a82cb604a4b555c26d1aba488f016d232a081f",
    "15cc89adf4e7ae5fa786245548e12504f8804479",
    NULL
};

static void compare_hash(BITMAPINFO *bmi, BYTE *bits, const char ***sha1)
{
    char *hash = hash_dib(bmi, bits);

    if(!hash)
    {
        skip("SHA1 hashing unavailable on this platform\n");
        return;
    }

    if(**sha1)
    {
        ok(!strcmp(hash, **sha1), "expected hash %s got %s\n", **sha1, hash);
        (*sha1)++;
    }
    else trace("\"%s\",\n", hash);

    HeapFree(GetProcessHeap(), 0, hash);
}

static void draw_graphics(HDC hdc, BITMAPINFO *bmi, BYTE *bits, const char ***sha1)
{
    HPEN solid_pen, orig_pen, dotted_pen, dashed_pen, dash_dotted_pen, dash_dot_dotted_pen;
    HBRUSH solid_brush, orig_brush, dib_brush;
    DWORD dib_size = get_dib_size(bmi);
    INT i;
    char brush_dib[sizeof(BITMAPINFO) + 256];
    BITMAPINFO *brush_bmi = (BITMAPINFO *)brush_dib;
    char *brush_bits = brush_dib + sizeof(BITMAPINFOHEADER);
    DWORD *bit_fields = (DWORD*)brush_bits, *dib_bits;
    DWORD ret;

    solid_pen = CreatePen(PS_SOLID, 1, RGB(0, 0, 0xff));
    orig_pen = SelectObject(hdc, solid_pen);

    memset(bits, 0xcc, dib_size);
    compare_hash(bmi, bits, sha1);

    /* solid lines */

    MoveToEx(hdc, 60, 10, NULL);
    LineTo(hdc, 60, 20);   /* vline t -> b */

    MoveToEx(hdc, 10, 70, NULL);
    LineTo(hdc, 100, 70); /* hline l -> r */

    MoveToEx(hdc, 100, 73, NULL);
    LineTo(hdc, 10, 73); /* hline r -> l */

    MoveToEx(hdc, 1, 70, NULL);
    LineTo(hdc, 1, 10);   /* vline b -> t */

    MoveToEx(hdc, 110, 20, NULL);
    LineTo(hdc, 120, 43); /* diag tl -> br */

    compare_hash(bmi, bits, sha1);

    memset(bits, 0xcc, dib_size);
    MoveToEx(hdc, 120, 43, NULL);
    LineTo(hdc, 110, 20); /* diag br -> tl */

    compare_hash(bmi, bits, sha1);

    /* Try the 16 ROP2s */
    for(i = 0; i < 16; i++)
    {
        memset(bits, 0xcc, dib_size);
        SetROP2(hdc, i+1);
        MoveToEx(hdc, 10, 20, NULL);
        LineTo(hdc, 110, 20);
        compare_hash(bmi, bits, sha1);
    }

    SetROP2(hdc, R2_COPYPEN);

    /* dotted lines 3 on 3 off */
    memset(bits, 0xcc, dib_size);
    dotted_pen = CreatePen(PS_DOT, 1, RGB(0,0x55,0));
    SelectObject(hdc, dotted_pen);
    SetBkMode(hdc, OPAQUE);
    SetBkColor(hdc, RGB(0,0,0xaa));

    MoveToEx(hdc, 10, 10, NULL);
    LineTo(hdc, 101, 10); /* l -> r */

    MoveToEx(hdc, 100, 12, NULL);
    LineTo(hdc, 9, 12); /* r -> l */

    MoveToEx(hdc, 10, 15, NULL);
    LineTo(hdc, 10, 80); /* t -> b */

    MoveToEx(hdc, 15, 80, NULL);
    LineTo(hdc, 15, 15); /* b -> t */

    compare_hash(bmi, bits, sha1);

    for(i = 0; i < 16; i++)
    {
        memset(bits, 0xcc, dib_size);
        SetROP2(hdc, i+1);
        MoveToEx(hdc, 10, 20, NULL);
        LineTo(hdc, 110, 20);
        compare_hash(bmi, bits, sha1);
    }


    SetBkMode(hdc, TRANSPARENT);

    for(i = 0; i < 16; i++)
    {
        memset(bits, 0xcc, dib_size);
        SetROP2(hdc, i+1);
        MoveToEx(hdc, 10, 20, NULL);
        LineTo(hdc, 110, 20);
        compare_hash(bmi, bits, sha1);
    }

    memset(bits, 0xcc, dib_size);

    SetROP2(hdc, R2_COPYPEN);


    /* dashed lines - 18 on 6 off. */
    dashed_pen = CreatePen(PS_DASH, 1, RGB(0,0x55,0));
    SelectObject(hdc, dashed_pen);
    SetBkMode(hdc, OPAQUE);
    SetBkColor(hdc, RGB(0,0,0xaa));

    MoveToEx(hdc, 10, 10, NULL);
    LineTo(hdc, 101, 10);
    compare_hash(bmi, bits, sha1);

    memset(bits, 0xcc, dib_size);


    /* dash dotted lines - 9 on 6 off 3 on 6 off. */
    dash_dotted_pen = CreatePen(PS_DASHDOT, 1, RGB(0,0x55,0));
    SelectObject(hdc, dash_dotted_pen);
    SetBkMode(hdc, OPAQUE);
    SetBkColor(hdc, RGB(0,0,0xaa));

    MoveToEx(hdc, 10, 10, NULL);
    LineTo(hdc, 101, 10);
    compare_hash(bmi, bits, sha1);

    memset(bits, 0xcc, dib_size);


    /* dash dot dotted lines - 9 on 3 off 3 on 3 off 3 on 3 off. */
    dash_dot_dotted_pen = CreatePen(PS_DASHDOTDOT, 1, RGB(0,0x55,0));
    SelectObject(hdc, dash_dot_dotted_pen);
    SetBkMode(hdc, OPAQUE);
    SetBkColor(hdc, RGB(0,0,0xaa));

    MoveToEx(hdc, 10, 10, NULL);
    LineTo(hdc, 101, 10);

    compare_hash(bmi, bits, sha1);
    memset(bits, 0xcc, dib_size);

    solid_brush = CreateSolidBrush(RGB(0xff, 0, 0));
    orig_brush = SelectObject(hdc, solid_brush);

    /* dash dot dotted Rectangle with solid brush */
    Rectangle(hdc, 10, 20, 50, 40);
    compare_hash(bmi, bits, sha1);
    memset(bits, 0xcc, dib_size);

    /* same rectangle with corners swapped */
    Rectangle(hdc, 50, 20, 10, 40);
    compare_hash(bmi, bits, sha1);
    memset(bits, 0xcc, dib_size);

    /* again */
    Rectangle(hdc, 50, 40, 10, 20);
    compare_hash(bmi, bits, sha1);
    memset(bits, 0xcc, dib_size);

    /* dib pattern brush */
    brush_bmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    brush_bmi->bmiHeader.biWidth = 9;
    brush_bmi->bmiHeader.biHeight = -2;
    brush_bmi->bmiHeader.biPlanes = 1;
    brush_bmi->bmiHeader.biBitCount = 32;
    brush_bmi->bmiHeader.biCompression = BI_BITFIELDS;
    brush_bmi->bmiHeader.biSizeImage = 0;
    brush_bmi->bmiHeader.biXPelsPerMeter = 0;
    brush_bmi->bmiHeader.biYPelsPerMeter = 0;
    brush_bmi->bmiHeader.biClrUsed = 0;
    brush_bmi->bmiHeader.biClrImportant = 0;

    bit_fields[0] = 0x07fc0000;
    bit_fields[1] = 0x0003fe00;
    bit_fields[2] = 0x000001ff;
    brush_bits += 12;

    memset(brush_bits, 0xff, 2 * 9 * 4);
    dib_bits = (DWORD*)brush_bits;
    dib_bits[0] = 0x07fc0000 | 0x1ff;
    dib_bits[1] = 0x07fc0000 | 0x3fe00;
    dib_bits[2] = 0;
    dib_bits[3] = 0x12345678;

    dib_brush = CreateDIBPatternBrushPt(brush_bmi, DIB_RGB_COLORS);
    ok(dib_brush != NULL, "ret 0\n");
    SelectObject(hdc, dib_brush);
    Rectangle(hdc, 1, 1, 30, 30);

    if(bmi->bmiHeader.biBitCount == 1) /* Need to learn how to dither brushes */
    {
        todo_wine compare_hash(bmi, bits, sha1);
    }
    else
        compare_hash(bmi, bits, sha1);

    memset(bits, 0xcc, dib_size);

    for(i = 0; i < 256; i++)
    {
        ret = PatBlt(hdc, 10, 10, 30, 30, rop3[i]);
        if(rop_uses_src(rop3[i]))
            ok(ret == FALSE, "ret %d\n", ret);
        else
        {
            ok(ret == TRUE, "ret %d\n", ret);
            if(bmi->bmiHeader.biBitCount == 1 && rop_uses_pattern(rop3[i])) /* Need to learn how to dither brushes. */
                todo_wine compare_hash(bmi, bits, sha1);
            else
                compare_hash(bmi, bits, sha1);
            memset(bits, 0xcc, dib_size);
        }
    }

    memset(bits, 0xcc, dib_size);

    SelectObject(hdc, orig_brush);
    SelectObject(hdc, orig_pen);

    DeleteObject(dib_brush);
    DeleteObject(solid_brush);
    DeleteObject(dash_dot_dotted_pen);
    DeleteObject(dash_dotted_pen);
    DeleteObject(dashed_pen);
    DeleteObject(dotted_pen);
    DeleteObject(solid_pen);
}

static void test_simple_graphics(void)
{
    char bmibuf[sizeof(BITMAPINFO) + 256 * sizeof(RGBQUAD)];
    BITMAPINFO *pbmi = (BITMAPINFO *)bmibuf;
    DWORD *bit_fields = (DWORD*)(bmibuf + sizeof(BITMAPINFOHEADER));
    HDC hdc, mem_dc;
    BYTE *bits;
    HBITMAP hdib, orig_bm;
    const char **sha1;

    hdc = GetDC(0);

    /* a8r8g8b8 */
    trace("8888\n");
    memset(pbmi, 0, sizeof(bmibuf));
    pbmi->bmiHeader.biSize = sizeof(pbmi->bmiHeader);
    pbmi->bmiHeader.biHeight = 100;
    pbmi->bmiHeader.biWidth = 512;
    pbmi->bmiHeader.biBitCount = 32;
    pbmi->bmiHeader.biPlanes = 1;
    pbmi->bmiHeader.biCompression = BI_RGB;

    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_a8r8g8b8;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);


    /* r8g8b8 */
    trace("888\n");
    pbmi->bmiHeader.biBitCount = 24;
    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_r8g8b8;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);

    /* r5g5b5 */
    trace("555\n");
    pbmi->bmiHeader.biBitCount = 16;
    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_r5g5b5;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);

    /* a8r8g8b8 - bitfields.  Should be the same as the regular 32 bit case.*/
    trace("8888 - bitfields\n");
    pbmi->bmiHeader.biBitCount = 32;
    pbmi->bmiHeader.biCompression = BI_BITFIELDS;
    bit_fields[0] = 0xff0000;
    bit_fields[1] = 0x00ff00;
    bit_fields[2] = 0x0000ff;

    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_a8r8g8b8;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);

    /* a8b8g8r8 - bitfields */
    trace("8b8g8r8 - bitfields\n");
    bit_fields[0] = 0x0000ff;
    bit_fields[1] = 0x00ff00;
    bit_fields[2] = 0xff0000;

    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_a8b8g8r8;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);

    /* a5r9g9b9 - bitfields */
    trace("5999 - bitfields\n");
    bit_fields[0] = 0x07fc0000;
    bit_fields[1] = 0x0003fe00;
    bit_fields[2] = 0x000001ff;

    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_a5r9g9b9;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);

    /* r5g6b5 - bitfields.  This one is optimized, so test independently. */
    trace("565 - bitfields\n");
    pbmi->bmiHeader.biBitCount = 16;
    bit_fields[0] = 0xf800;
    bit_fields[1] = 0x07e0;
    bit_fields[2] = 0x001f;

    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_r5g6b5;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);


    /* r5g5b5 - bitfields.  Should be the same as the regular 16 bit case. */
    trace("555 - bitfields\n");
    pbmi->bmiHeader.biBitCount = 16;
    bit_fields[0] = 0x7c00;
    bit_fields[1] = 0x03e0;
    bit_fields[2] = 0x001f;

    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_r5g5b5;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);

    /* r4g5b4 - bitfields */
    trace("454 - bitfields\n");
    pbmi->bmiHeader.biBitCount = 16;
    bit_fields[0] = 0x1e00;
    bit_fields[1] = 0x01f0;
    bit_fields[2] = 0x000f;

    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_r4g5b4;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);

    /* 8 bit */
    trace("8\n");
    pbmi->bmiHeader.biBitCount = 8;
    pbmi->bmiHeader.biCompression = BI_RGB;
    pbmi->bmiHeader.biClrUsed = 5;
    pbmi->bmiColors[0].rgbRed = 0xff;
    pbmi->bmiColors[0].rgbGreen = 0xff;
    pbmi->bmiColors[0].rgbBlue = 0xff;
    pbmi->bmiColors[1].rgbRed = 0;
    pbmi->bmiColors[1].rgbGreen = 0;
    pbmi->bmiColors[1].rgbBlue = 0;
    pbmi->bmiColors[2].rgbRed = 0xff;
    pbmi->bmiColors[2].rgbGreen = 0;
    pbmi->bmiColors[2].rgbBlue = 0;
    pbmi->bmiColors[3].rgbRed = 0;
    pbmi->bmiColors[3].rgbGreen = 0xff;
    pbmi->bmiColors[3].rgbBlue = 0;
    pbmi->bmiColors[4].rgbRed = 0;
    pbmi->bmiColors[4].rgbGreen = 0;
    pbmi->bmiColors[4].rgbBlue = 0xff;

    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_8;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);

    /* 4 bit */
    trace("4\n");
    pbmi->bmiHeader.biBitCount = 4;

    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_4;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);

    /* 1 bit */
    trace("1\n");
    pbmi->bmiHeader.biBitCount = 1;
    pbmi->bmiHeader.biClrUsed = 2;

    hdib = CreateDIBSection(0, pbmi, DIB_RGB_COLORS, (void**)&bits, NULL, 0);
    ok(hdib != NULL, "ret NULL\n");
    mem_dc = CreateCompatibleDC(hdc);
    orig_bm = SelectObject(mem_dc, hdib);

    sha1 = sha1_graphics_1;
    draw_graphics(mem_dc, pbmi, bits, &sha1);

    SelectObject(mem_dc, orig_bm);
    DeleteObject(hdib);
    DeleteDC(mem_dc);

    ReleaseDC(0, hdc);
}


START_TEST(dib)
{
    CryptAcquireContextW(&crypt_prov, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);

    test_simple_graphics();

    CryptReleaseContext(crypt_prov, 0);
}
