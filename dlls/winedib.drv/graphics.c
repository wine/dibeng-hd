/*
 * DIB Engine graphics apis.
 *
 * Copyright 2008 Huw Davies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

#include <stdarg.h>
#include "windef.h"
#include "winbase.h"
#include "wingdi.h"

#include "dibeng.h"

#include "wine/debug.h"

WINE_DEFAULT_DEBUG_CHANNEL(dibeng);

static inline void order_int(int *i1, int *i2)
{
    if(*i1 > *i2)
    {
        int tmp;
        tmp = *i1;
        *i1 = *i2;
        *i2 = tmp;
    }
}

/***********************************************************************
 *           LineTo
 */
BOOL DIBENG_LineTo(dibeng_pdevice_t *pdev, INT x, INT y)
{
    POINT cur_pos;

    GetCurrentPositionEx(pdev->hdc, &cur_pos);

    reset_dash_origin(pdev);

    if(cur_pos.y == y) pdev->pen_hline(pdev, cur_pos.x, x, y);
    else if(cur_pos.x == x) pdev->pen_vline(pdev, x, cur_pos.y, y);
    else pdev->pen_line(pdev, cur_pos.x, cur_pos.y, x, y);

    return TRUE;
}

/***********************************************************************
 *           Rectangle
 */
BOOL DIBENG_Rectangle(dibeng_pdevice_t *pdev, INT left, INT top, INT right, INT bottom)
{
    int i;

    order_int(&left, &right);
    order_int(&top, &bottom);

    reset_dash_origin(pdev);
    /* Draw the perimeter starting at the top-right corner and move anti-clockwise */
    pdev->pen_hline(pdev, right - 1, left - 1, top);
    pdev->pen_vline(pdev, left, top + 1, bottom);
    pdev->pen_hline(pdev, left + 1, right, bottom - 1);
    pdev->pen_vline(pdev, right - 1, bottom - 2, top);

    for (i = top + 1; i < bottom - 1; i++)
        pdev->brush_hline(pdev, left + 1, right - 1, i);

    return TRUE;
}

static inline BOOL rop_uses_src(DWORD rop)
{
    return (((rop & 0xcc0000) >> 2) != (rop & 0x330000));
}

/***********************************************************************
 *           get_rop2_from_rop
 *
 * Returns the binary rop that is equivalent to the provided ternary rop
 * if the src bits are ignored.
 */
static inline DWORD get_rop2_from_rop(DWORD rop)
{
    return (((rop >> 18) & 0x0c) | ((rop >> 16) & 0x03)) + 1;
}

/***********************************************************************
 *           PatBlt
 */
BOOL DIBENG_PatBlt(dibeng_pdevice_t *pdev, INT left, INT top, INT width, INT height, DWORD rop)
{
    INT i;
    DWORD old_rop2;

    TRACE("%p %d,%d %dx%d %06x\n", pdev, left, top, width, height, rop);

    if(rop_uses_src(rop)) return FALSE;

    old_rop2 = SetROP2(pdev->hdc, get_rop2_from_rop(rop));

    for (i = 0; i < height; i++)
        pdev->brush_hline(pdev, left, left + width, top + i);

    SetROP2(pdev->hdc, old_rop2);

    return TRUE;
}
