/*
 * DIB Engine header.
 *
 * Copyright 2008 Huw Davies
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
 */

typedef struct
{
    int bit_count, width, height;
    int stride; /* stride in bytes.  Will be -ve for bottom-up dibs (see bits). */
    void *bits; /* points to the top-left corner of the dib. */

    DWORD red_mask, green_mask, blue_mask;
    int red_shift, green_shift, blue_shift;
    int red_len, green_len, blue_len;

    RGBQUAD *color_table;
    DWORD color_table_size;
    struct primitive_funcs *funcs;
} dib_info_t;

typedef struct
{
    DWORD count;
    DWORD dashes[6];
} dash_pattern_t;

typedef struct dibeng_pdevice
{
    HDC hdc;
    dib_info_t dib;

    /* pen */
    COLORREF pen_colorref;
    DWORD pen_color, pen_and, pen_xor;
    const dash_pattern_t *pen_pattern;
    DWORD cur_dash, left_in_dash;
    enum mark_space { mark, space } mark_space;

    /* brush */
    COLORREF brush_colorref;
    DWORD brush_color, brush_and, brush_xor;
    dib_info_t brush_dib;
    void *brush_and_bits, *brush_xor_bits;

    /* bkgnd */
    DWORD bkgnd_color, bkgnd_and, bkgnd_xor;

    void   (* pen_hline)(struct dibeng_pdevice *pdev, int start, int end, int row);
    void   (* pen_vline)(struct dibeng_pdevice *pdev, int col, int start, int end);
    void    (* pen_line)(struct dibeng_pdevice *pdev, int x1, int y1, int x2, int y2);
    void (* brush_hline)(struct dibeng_pdevice *pdev, int start, int end, int row);
} dibeng_pdevice_t;

typedef struct primitive_funcs
{
    void         (* solid_hline)(const dib_info_t *dib, int start, int end, int row, DWORD and, DWORD xor);
    void       (* pattern_hline)(const dib_info_t *dib, int start, int end, int row, const void *and, const void *xor, DWORD count, DWORD offset);
    void         (* solid_vline)(const dib_info_t *dib, int col, int start, int end, DWORD and, DWORD xor);
    void *     (* get_pixel_ptr)(const dib_info_t *dib, int x, int y);
    void           (* set_pixel)(const dib_info_t *dib, int x, int y, DWORD and, DWORD xor);
    DWORD      (* get_pixel_rgb)(const dib_info_t *dib, int x, int y);
    DWORD  (* colorref_to_pixel)(const dib_info_t *dib, COLORREF color);
} primitive_funcs_t;

extern primitive_funcs_t funcs_8888;
extern primitive_funcs_t funcs_32;
extern primitive_funcs_t funcs_888;
extern primitive_funcs_t funcs_555;
extern primitive_funcs_t funcs_565;
extern primitive_funcs_t funcs_16;
extern primitive_funcs_t funcs_8;
extern primitive_funcs_t funcs_4;
extern primitive_funcs_t funcs_1;

extern void reset_dash_origin(dibeng_pdevice_t *pdev);
extern BOOL init_dib_from_packed(dib_info_t *dib, void *packed);
extern void copy_dib_color_info(const dib_info_t *src, dib_info_t *dst);
extern void convert_dib(const dib_info_t *src, dib_info_t *dst);

extern BOOL DIBENG_CreateBitmap( dibeng_pdevice_t *pdev, HBITMAP hbitmap, LPVOID bmBits );
extern BOOL DIBENG_CreateDC( HDC hdc, dibeng_pdevice_t **pdev, LPCWSTR driver, LPCWSTR device,
                             LPCWSTR output, const DEVMODEW* initData );
extern BOOL DIBENG_LineTo(dibeng_pdevice_t *pdev, INT x, INT y);
extern BOOL DIBENG_PatBlt(dibeng_pdevice_t *pdev, INT left, INT top, INT width, INT height, DWORD rop);
extern BOOL DIBENG_Rectangle(dibeng_pdevice_t *pdev, INT left, INT top, INT right, INT bottom);
extern HBITMAP DIBENG_SelectBitmap( dibeng_pdevice_t *pdev, HBITMAP hbitmap );
extern HBRUSH DIBENG_SelectBrush( dibeng_pdevice_t *pdev, HBRUSH hbrush );
extern HPEN DIBENG_SelectPen( dibeng_pdevice_t *pdev, HPEN hpen );
extern COLORREF DIBENG_SetBkColor( dibeng_pdevice_t *pdev, COLORREF color );
extern INT DIBENG_SetROP2( dibeng_pdevice_t *pdev, INT rop );
