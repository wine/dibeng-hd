@ cdecl CreateBitmap(ptr long ptr) DIBENG_CreateBitmap
@ cdecl CreateDC(long ptr wstr wstr wstr ptr) DIBENG_CreateDC
@ cdecl LineTo(ptr long long) DIBENG_LineTo
@ cdecl PatBlt(ptr long long long long long) DIBENG_PatBlt
@ cdecl Rectangle(ptr long long long long) DIBENG_Rectangle
@ cdecl SelectBitmap(ptr ptr) DIBENG_SelectBitmap
@ cdecl SelectBrush(ptr ptr) DIBENG_SelectBrush
@ cdecl SelectPen(ptr ptr) DIBENG_SelectPen
@ cdecl SetBkColor(ptr long) DIBENG_SetBkColor
@ cdecl SetROP2(ptr long) DIBENG_SetROP2
